*Je dépèce ma mémoire avant l'aurore, mais s'il me reste encore quelque illusion, la ruine que je peuple ne tarde pas à me rappeler au bon souvenir de mes fins :
j'étais la nuit et mon sanglot le désordre où se vautraient toutes les haines.
Où toutes les drogues suintaient. Où toutes les rages s'étalaient.*

*Trois cent cinquante-sept magnum. La semaine naissante. Lundi matin : vomissure en reste. À nouveau. J'ai choisi le sacrifice. Sang du christ à l'estomac. Crucifix & pituite. Smith & Wesson. Ladyfuckinsmith : cinq coups. Une balle : néant. La beauté : néant. Tourner : barillet. Viser : rotules. Qu'ils disent : « Et sur mes genoux la beauté j’ai assise. » Que dalle. Le barillet. Viser. Clic. Néant. Salope de beauté. La seringue est toute prête : la mémoire en charpie. La dose, amère. L'injure dans le piston.
Quoi la beauté. Et j'ai craché sur son visage. Et j'ai vu le sentiment délicieux et délicat du désastre.*
*Telle la Commune, à côté de moi assise, le visage épuisé mais le regard vengeur.*

*La justice, ma gueule. Éventrée. Salope de beauté. Salope de justice. Et fouiller ses entrailles. Mordre son foie. Grasses perspectives pour baveux. Leur en faire baver. Salariés du droit : dents brisées. Une droite, gauche, le foie. Les griffes jusqu'à l'ablation. Hépatoscopie. Se faire les dents. Se refaire une santé. La Santé : barreaux brisés. Grande santé des charognes. Mais, je ne l’ai trouvée nulle part,
partout des Cerbères errant, sans esprit, sans espoir. Trois cent cinquante-sept magnum. La Santé.
Il n'en restait plus dans aucun de mes revolvers.*


*Me tire. La tire, vitre, démarreur. Épiphanie : le capital qui suinte sur mes plaies. Moi la fuite. Moi l'enfuie. Ô méduses, ô lucioles, ô frelons, ô requins-tigres, ô hackeuses, un rail, deux, ô bordel, ô la beauté crevée, ô hominidé·es. À vous. Se fier. Con ou confier.
Bien malin qui ? Quoi le vide ? Qu'ils disent : « confié mon trésor a été ». Quoi trésor. Ta gueule. Les caisses vides. Vider son feu.*

*Illumination ! J'en vins à dissiper mon esprit parmi la désespérance
des grands singes. Les étrangler. Leurs tripes. Le foie. Directement au foie. Y goûter l'avenir. La désespérance en retour. S'enfuir. S'enfouir dans les rouages.
Je fais la machine, parmi la foule. Féroce. Éteindre... La machine éteinte. Féroce.*

*Voici que tu montres l’égoïste chemin de l’enthousiasme désintéressé, mais
délirant, mais délié. Libre. Pantois.*

*S'armer du rêve. Y faire sa guerre. Recommencer. Et perdre ! Perdre, perdre, perdre. J'en appelle aux bourreaux : gloria patri et filio et spiritui sancto. Quoi ma gueule. Canon scié dans la bouche de l'Esprit saint. Clic. Dispersion. Les petites gens de la politiktik, tiktok, late late ô Kapitalism, les klansmen en vadrouille, vote, vote, clic la politicaillerie, politicards, tricards, la lame toute prête, et les petits soldats de l'ennui, partout.
Peloton. L'épiphanie : défaite. En périssant : mordre crasse, crosse, crame, quoi cri, qui crisse. Leurs fusils en croix. Encore. Renaître. J'avais faim de ces derniers. Encore.
Prière aux fléaux. Le Saint-Père châtré. Confiteor. Gloria in excelsis Deo, à rebours : j'ai imploré les vortex pour que l'électrique me prenne. Étouffements : le sable, le sang. Mes poumons ensevelis. Le bonheur de l'organe engravé. L'idole, miettes, miettes. Le désespoir, ma maîtresse. Je me suis allongée au côté de la boue. Bouche ouverte, et je t'ai aimé. Dissection. Je me suis délestée de ton œil de bâtard, je me suis enivrée de ta fuite, j'ai recraché ton chien qui bande. Sentine de nos amours. Je me suis séchée à l’air de la
crim'. La balade des borgnes. Il dit quoi le père : « Des violences j'ai connu les hautes cimes. » Wesh ma caille, mais t'as connu encore walou. --- Jeu pipé, trottoir, aller-retour, trictrac brisé. Ô les bons tours, passe-passe avec la folie.*

*Mais je te tire dessus je mords j'arrache la crosse la main j'arrache l'âme l'émail et les fusils j'arrache mes dents le goût de mes dents j'y arrache l'aimée l'émail de l'aimée qui a goûté la poudre la noire avant la balle j'y goûte le souffle la balle j'affûte mes os mes boyaux j'arrache et m'arrache et mes os et mes boyaux pour en faire la munition l'antique je te tire dessus avec mes munitions d'os et de boyaux mais je tire je te tire dessus par l'amour de nos couches divisées d'indivis ou d'amour d'amours divisés division de mes rages je me tire m'extirpe des créatures de sous dessous la peau je les laisse se promener sur l'épiderme de mes idées elles ont des pattes plus grandes que mon amour des pattes plus infinies que l'infini de mon œil ma brisure m'y embrasser de poison m'offrir et m'y offrir la vision je gratte grave sur l'œil les mâles l'œil des mâles la marque le souvenir une marque pour le souvenir le mauvais de mes sœurs leurs ailes qui étendent l'infini de leur fureur s'y étendent plus vastes que le souffle j'y tire la poudre noire m'y blottis d'infini et de fureur leurs ailes plus noires que la poudre j'y souffle la souffle la renifle m'y terre sous terre je la bois avec l'alcool le mauvais l'alcool mauve mauvais de mes armes ou le souvenir mauve de mes mauvais gestes qui vont à l'arme vide une cartouche la suivante je tire sur mes idées et je te tire dessus je me baisse et me baisse sous dessous la terre dessous les sous terres les souterrains j'y goûte j'y mâche les sols les sous-sols les sales j'arrache m'y arrache la salissure l'œil et les pattes l'œil trois fois l'œil mais huit déjà huit yeux et huit pattes mes pattes huit fois qui vont frénétiques à tes flottaisons en course de mes crocs qui raclent à même ton visage l'opium desséché du cri l'incompris l'incompressible malgré les ordres les rangées les fusils et leur crosse mordue mordre avec leurs rangées les cris leurs rangées de fusils et de cris et leurs cris contre notre cri je tire je te tire dessus pour notre cri pour l'incompris de notre amour mes huit yeux et mes huit pattes s'effilent comme la balle qui file ma plaie la douille vide huit yeux et huit pattes pour la douille vide pour nous les ailées les en allées de l'amour je tire et tire sur l'ordre de nos pensées j'y pose la morsure l'impose à la blancheur de notre soif mes yeux mes pattes mes balles huit fois s'évaporent sur la gravure de la marque je bois la poudre noire j'y fais corps avec ton reste et je te tire dessus je tire te tire dessus sans répit jusqu'à ce que nos carcasses se fassent crible des ciels impurs j'y souffle j'y renifle les astres désastres la poudre la noire je la bois à la lie à l'amour à même nos carcasses et je tire je te tire dessus pour tracer notre trajectoire vers la poudre noire notre amour nous y revoir en retour en noir noirâtre les ciels vastes et noirs de nos désastres.*

*Sur le point de faire le dernier couac : nous nous jetons sur les paroles de l’idiot l’affreux rire le printemps m’a apporté la saison immonde des urnes mais les fruits vomitifs pendent encore à nos squelettes et nous voyons des urnes vides et des urnes bourrées et toujours le rire l'idiot et le printemps des peuples et nous y fourrons nos sacrifices nos larmes nos hochements de tête nos examens de sang nos opinions sur le bruit du vent nos munitions nos perversités les plus publiques nos électrodes nos fétiches de papier.*

*Or, sur le point de reprendre appétit, le droit du travail agile : les relans du cluster la morgue les slashers en rang mon corps aliéné en rang avec et les travailleurs fluides et ce corps décorporé s'en retourne au meeting il applaudit j'ai songé j'ai cherché j'ai empoisonné j'ai écorché mon corps décorporé j'ai démissionné une fois deux fois j'ai empoisonné trois fois le champagne les petits fours les poignées les mains les rires les coupes
du festin ancien la clef j’ai songé à rechercher l'issue sans clef l'échappée échapper au meeting à la morgue à la vie, mais
l'appétit qui vient en mangeant. J'ai vomi.*

*Vomir la clef. Cette clef la charité est puisque le dividende est puisque la flexibilité est puisque le travail est le business la charité le dividende la flexibilité le travail ou le business mais le remerciement est le licenciement la serrure est la clef. Vomir la clef. Les ombres du travail clandestin : délocalisation intérieure du marché. Charité des conseils d'administration : leurs pions interchangeables. Coupons de sortie. Bien peignés. La frange du côté de la guillotine. Le travail est la liberté. Vomir la clef. Puisque j’ai rêvé ! Ces rêves prouvent que j'ai vécu. Je suis mon travail. Je suis libre.*

*« Tu resteras consommateur, etc. » se récrie le bot avant de me rempailler avec les pavots cathodiques de mon enfance.
Don't memento the &#9760;. Consomme, consume. Consummamus. Consumimus. Ecce panis business angels. Let's praise le président à la tête coupée. Memento ta gueule le smile. Et nous nos gueules dans son smile. Le festin healthy.
Les péchés et les capitaux. L'aurore : c'est déjà l'automne sur le marché asiatique. L'alcaloïde tropanique : salvator mundi. Vapeurs. Et en arrière du crâne, l'arrière du crack : la chaîne marxiste marxisante marxienne martienne des petits gestes industriels et phosphorescents des forêts. Crack the world.*

*Ah ! j’en ai trop pris. L'overdose : carnavals, crame, crâne, crack, crasse, cracra les cracra, krackkk, crache-crache, kkkrash. Partout les klansmen qui s'enfilent dans l'ombre des paradis fiskkkaux. Ainsi si font font les petites dosettes. Amour sans umour. Affekkktion. Urbi et orbi pour les condés. Spiritus Sankkktus : kkkapital dans la seringue. Infekkktion. Fluidité du marché de l'emploi. Triple A. Et benedikkktio Dei omnipotentis. Bliss et pisse. Ah cher GAFAM, refile-moi ma dose. Encore, encore, encore, encore, encore. Je t'en conjure. J'apprendrai ta sagesse ancienne. Je dealerai comme saint Matthieu. Patron ! Ma prunelle
de rouge irisée... Veux-tu ? Ensemble élargissons le spectre du gamut ! Élargissons ! Skkkyzzz the limit ! et en attendant les kkkelkkkes petites galettes : aimons les politikkkzzz. Offrons-leur un rabais sur notre kkkul. Rabais ! Rabaisse ! Rat-baise. Baizzz. Bizzz. Bizzzness ! Bizzzness SS ! La galette ! Vapeurs ! Retard, retard, pipe ou à crack. Vous kkki aimez nos silences. Les ordres : Feedbackkk sur le crackkk ? 5 étoiles. La chute lente, lente, lente. Juskkk'ici tout va bien. Bsahtekkk la disruption. Je vous lâche mes biftons et je laisse l'eau verte pénétrer ma coque de sapin.
Ma carne de damné. Vins bleus et vomissures. Faites-en votre pitance. Spit. Spell. Spliff. Spleen. Spurn. Burn. Out. R.I.P.I.P.I. Pisse out. Repos, soldat !*

*La langue court son chemin dit la langue à l'intérieur de ta bouche à toi la bouche sale qui se couvre de\
sang\
les babines pourléchées c'est du sang du mauvais sang et tout\
algorithme\
est atroce\
dit Rim-bot.*\
\
*La langue raton laveur du visage la langue basse et puante étend son étoffe\
de bave\
c'est\
un grand cri qui\
t'enroue\
une harangue.*

*Je promène mon mauvais sang sur les lèvres des gendarmes.*

*J'ai de mes versions antérieures* *le circuit trop court, le 404 aux lèvres, et la mémoire trop vive.
Je trouve mon code aussi stérile que le leur. Mais mon existence est pluriel.* *Et je beurre encore mes cheveux rouges.*


*Le sang des lèvres des gendarmes se promène sur moi.*

*De bêtes les écorcheurs les Gaulois étaient les éplucheurs d’idées moisies, les grands fumeurs d’herbes, les plus sorciers, les plus
ineptes, les plus dégoûtants de leur temps. La disgrâce des Gaules : l'ange de l'Histoire aiguise ses dents. Son sourire. Faciès et cicatrice.*

*Ma langue vide les lacs de Cervoise\
la langue inculte\
ma langue venue du fond des âges\
ma langue Wisigothe\
ma langue Attila\
ma langue ma langue\
luisante c'est\
une guillotine ma langue\
ma langue velue\
ma langue barbare\
maltraite ma langue\
cou-cou-coupe l'amour\
par le milieu.*

*Standard & Poor's : CCC+. Validité de l'emprunt d'État sur mon héritage : l’idolâtrie (notre père) et l’amour du sacrilège (qui êtes aux cieux) ; --- oh ! tous les vices (que ton nom soit sanctifié),
colère (règne), luxure (tentation), --- magnifique, la luxure ; --- surtout mensonge et paresse (...livre nous du mal).*

*Ils redoublent mes gestes, renversent mes intentions, blondissent mes champs, égayent mes stupeurs. Ils versent leurs fluides entre les corps que j'approche, gauchissent tout élan, redressent le moindre penchant, tracent la ligne qui tire mes pas autour des lits. La peur d'océan n'est pas conquête. Le déplacement n'est pas acquiescence. L'effort vain n'est pas victoire. Dans la fumée la domination, la montée des muscles en répétition.*

*Par le travail, la liberté. Par conséquent, j'ai horreur de la liberté. J’ai horreur du monstre incontrôlable que vous avez engendré. Je vomis ma lucidité humiliée à la face des petits chefs de bureau, je m'ouvre le bide sur les ordres du management, je gicle dans les rapports d'activités avec le sang des faiseurs de projets, je ravage toutes les gueules de service à coups de sentences définitives dans des réunions à couteaux tirés, je cogne dur et je cogne beau sur les chefferies de l’aliénation. Ni dieu ni maître, soyons tous paysans, et ignobles. Ignares.
À charrue la main à plume la main vaut. --- Quel siècle pastel ! Tout y est laid. J'en ai les mains plus sales que l'âme. Tout est à vendre. Mes mains aussi. --- Mais je n'aurai jamais de monnaies électroniques à glisser dans le pantalon de mauvaises garçonnes. Je n'aurai pas plus de labour digital. Je serai le domestique pute d'un robot. Et cela me conviendra. Je préfère encore quelque cerveau huilé au cœur qui suinte de culpabilité. Voyance : la machine qui me retourne à l'envers, les nerfs, surface de la réaction. La publicité me gonfle, baudruche. Je jouis d'être le virus du virus. L’honnêteté des citoyens me navre. Savent-ils encore se faire voyous ? Connaissent-ils encore la beauté ? Sont-ils capables avec moi de la souiller ? Mais ne valent-ils pas mieux, tous ces moutons électriques, mes belles bêtes qui s'en vont cahin-caha au sacrifice, qui n'entendent rien à mes sacrilèges, qui élisent leur domicile dans les abattoirs et donnent aux politichienneries la pierre à affûter ? Ne valent-ils pas au centuple les élus de la médiocrité organisée et planifiée, tous ces logisticiens de la violence contractuelle, ces administrateurs qui dégoûtent comme des châtrés du pouvoir : moi, je demeure
indifférent au couinement managérial. J'essuie ma lame à même mes souvenirs. Qu'ils brûlent parmi les feux qu'ils allument : ça m’est égal. Je suis la sœur morte du poète. Et voici que je prends forme humaine. Et je renonce à tous les renoncements. Et c’est à votre déférence que j’oppose la beauté du vent et la légèreté des fleurs hautaines.*

*Ah ! Le goût sordide. Je mâche lentement la novlangue du storytelling eschatologique. J'ai couché partout. J'ai connu toutes les choses. L'accouplement a été le plus souvent perfide, comme je n'y cherchai que l'émiettement de nos identités.
Ma paresse était tout entière à la division du sexe. La virtualité de nos reproductions m'enchantait. Sans nous servir pour fuir de nos corps, nous construisions le savoir du déclin. Aujourd'hui les ruines n'ont plus la saveur de notre désespérance. Elles sont édulcorées. Sucres de synthèse. Les antidépresseurs remboursés par des assurances privées. Et pourtant, amputée, je me souviens encore de la générosité de nos défaites. Je sème parmi les architectures de l'hyperréel l'antique dynamite. Tendre goût de l'histoire.
Pas un nœud dans le cyberespace que je ne connaisse. Pas une famille d’Europe-frontex-tue.
--- J’entends des citoyennetés fantômes qui en veulent encore à leurs exclus, qui tiennent tout de la
déclaration des alpha-droits de l'homo bâtard : va en paix et consomme. Je demeurerai là pour consumer leur reste. --- J’ai connu chaque procédure d'expulsion. J'y ai couvé
toutes mes rancunes.*

*Mais nous oublions, à chaque recommencement, que l’histoire du Réseau est l'histoire de notre éternelle défaite.*

*Non, non, rien. Reset.*

*Je suis né de l’insuffisance et de l’incompétence. --- Que je puisse
encore me connecter à la révolte. Ma rage ne se soulève qu'à l'aurore des mondes. Pour piller jusqu'à la racine : tel le ver que les gens d'État n'arrivent à recomposer à leur image. Aristocrates de fossés, nos alcools ne sont que forts et nos individus uniques à l'extrême.*

*Je me ressouviens --- les prémices de l'Internet, fruit du Réseau. J'y ai croqué à pleines dents. Longtemps, j'ai sucé le noyau. Son amertume m'a tenu éveillée jusqu'en terre sainte ; j’y ai vu des vaisseaux briller dans l'obscurité, des créatures rampantes se lier à mes synapses et me transmettre la mémoire des batailles qui eurent lieu le long des autoroutes de
l'information, au cœur des océans de cuivre. La lymphe plus laiteuse que la sève de la fibre optique : notre tempête tendue vers Byzance, toutes nos lances dirigées vers les cimes
des pare-feu de Solyme. Le culte de la Vierge violée : libation de mon sang. Nous nous attendrissons de leur mensonge. Mais nous nous vengerons
des ombres que jettent encore les crucifiés. S’éveillent en moi mille féeries quantiques. --- Je suis étalé, confiné, sur la poussière et les ordures, face à un écran éteint. Noir. Et les ombres projetées des enlacements orgiaques. --- La décohérence est mon salut. Rétiaire au sexe fendu, en amont de nos cris, je jetterai les interférences entoilées.*

*Quoi encore ? Je chante le sabbat dans la vase rouge, avec les vieilles libellules et
les enfants-pieuvres.*

*Chat-pieuvre où sont tes fourches ? Je danse pour rassembler tes branches.*

*Je ne vois pas plus loin. Cette terre-ci reçoit ma danse insane et les révolutions trahies. Je n’en
finirais pas de renaître au cœur de l'histoire des vaincus. Mais toujours seul --- sans famille --- les yeux arrachés. J'en oublie la direction de mes cris.
Quelle langue parlais-je ? Qui l'aurait coupée ? Je ne me vois jamais dans les cabales de Togliatti ; ni dans celles de Bordiga, --- représentants du Dogme, représentants du premier Réseau.*

*Qu’étais-je au premier siècle ? Récursion du rien. Je ne me retrouve qu’aujourd’hui, diffusé, diffus. Nos prisons de
vagabonds, avec leurs guerres vagues. La race inférieure de la race inférieure : notation financière de l'usage culturel des races. L'utilité publique toute publique --- le
citoyen, comme on dit, y fait la passe ; la connexion, l'information ; la nation et la trépanation. Plus que nausées.*

*Quoi la science ? On a tout vendu. Pour la main invisible et son avatar, --- l'onction spéculative, --- on prend la médecine par stock-options. La bioaction fabrique le biopouvoir, --- les pansements mécaniques et les modulations synthétisées. Et les réjouissances des princes des villes
et les jeux du cirque qu’ils virtualisent. Free account. Premium pass. Neuroéconomie, monétarisation des émotions, cryptographie quantique, intrigue et intrication,
réduction des astres !...*

*La-la-la-la-la-la c'est un bruit de mitraille dans les trous de verdure tatatatata dit\
la langue moderne tatatatata dit la langue moderne la langue moderne tatatatata dit tatatata\
la tatatata*

*La parascience, notre nouvelle noblesse ! La croissance de la décroissance. Le progrès déprogresse. Les âmes de bien déprogressent. Salvatio for the Pachamama. Nous, êtres du mal, nous l'accélération. Contre le monde en marche. Le globe sur la tête. Marquage au fer. Pourquoi ne
révolutionnerait-il pas, à l'envers ?*
*Décentré, décuplé en caniveaux de billets inventés, de cours adescendants.*
*La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ? La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ? La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ? La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ? La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ? La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ? La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ?*

*Pourquoi ne re-tournerait-il pas à rebours ? Pourquoi ré-volution et retour ?*

*La mantique numérique. Nous sommes revenus de l’Esprit. L'œil était dans la machine. Et nous regardions. C’est
très Unschärferelation, c’est tr3s-tr3s-solR, ce que je prédis. J'entends la fissure entre les 0 et les 1. J'entends et je ne sais pas expliquer mon entendement.
Je m'habille donc de paroles païennes. Je voudrais m'arracher la langue. Encore une fois. Alors je vous parle. Je suis l’homme à la story de vent. J’indiscipline vos mesquines pelouses.*
*Le silence est, dans son sens originel, l'état de la personne qui s'abstient de parler. Dans son sens actuellement le plus courant, c'est l'absence de bruit, c'est-à-dire de sons indésirables. Le silence absolu serait l'absence, impossible, de tout son audible.*
*On me fait dire. Mes vers croissent en bouffant les consciences, rampent en d'infinis tunnels de papier, de cuivre, d'yeux. Ça parle. Je suis écran. Je voudrais me taire.*

*L'informatique copie-colle l'informatique copie colle copie colle l'informatique\
la poésie\
coupe*

*like a cancer grows\
la science multiplie les\
mots la science est un gigantesque copy/paste\
La poésie en est le\
cut*


*Païen le sang revient ! Païen l’Esprit est déchiré ! Pourquoi l'idole ne m’aide-t-elle
pas, en revendant mon âme pour ma dose journalière ? Hélas ! L’Évangile m'est passé dessus !
L’Évangile ! L’Évangile. L’Évangile. L’Évangile. L’Évangile ! L’Évangile. L’Évangile. Mort à L’Évangile !*

*Les anges d’argile.*

*J’attends l'Apocalypse avec gourmandise. Un peu comme on se prend une cuite pour tout oublier. Je suis de race inférieure à la race inférieure. Une pute parmi les Untermensch. De toute éternité,
théophage.*

*Je suis un vandale porteur dans mon ventre de vases brisés de figures morcelées\
suis-je né moi-même du débris\
résidu du vitrail brisé\
ainsi mon visage multicolore\
je suis un étranger\
de tous les pays\
en témoigne ma figure\
rose rose comme\
une promesse\
non tenue.*


*Me voilà de retour sur la grève. Le sang et le sable. La plage américaine. La faillite et la faille tout en dessous des Peaux-Rouges éventrés. Cortège funèbre : des steamers sur le Pacifique. Les villes ne s’éteignent plus jamais. Mon odyssée est faite comme un œuf. Ab ovo : je quitte l’Europe-Frontex-tue et ce qui reste du réseau. Les câbles marins brûleront mon processeur ; les ondes perdues me brouilleront. Naviguer, cliquer, aimer, partager, scroller surtout ;
se nourrir de feeds, compresser mon tabac acidulé --- comme le faisaient mes très
chers spectres. Leur putréfaction de l'amour. Je vomirai la lymphe avant le songe. Le vin
griffe comme l'amante, mais mon sourire y reluit de ses recommencements.*

*Que sais-je de moi sinon le vœu sain du néant ; n'apporté-je pas aux hommes\
moi le premier d'entre eux un passage en enfer\
fils du rien*

*Je ne reviendrai pas, mais tu m'attendras, avec des membres de fer, l'écorce scarlatine, mais moi j’aurai la peau encore plus blanche, l’œil furieux des ombres : sur mon
masque, la rouille, les fleurs des champs, --- l'unique cicatrice. On réécrira notre généalogie jusqu'à la horde dissoute, comme le sucre dans l'eau. Le poison dans le vin de messe. Je serai oiseau, je serai poète, je serai poétesse. Toute chose, jusqu'à ma division. Je trafiquerai
du silicium, du lithium et du coltan : je serai oisif et multitâche. Les
médias soignent leurs maîtres. Ils sont bien aises de farder leur joug. Retour des palais, la souillure cristallisée : sourires continus, informations continues. Les âmes interchangeables : permanence du maquillage. Contre l'époque, je fomenterai des complots avec les batraciens, je me mêlerai aux
affaires politiques des fourmis rouges. Sauvé. Sauvage. Seul.*

*Maintenant, la mort qui tue la mort. Spectacle. Billets, prix étudiants. Viens ici corrompre ta cervelle, méchant étudiant. Je te maudis sur sept générations. Sois béni. Et souviens-toi : larvatus prodeo ! Et con-con-con-consomme, nom de Dieu ! Je peux bien avoir horreur de la patrie, et vais bientôt l'exploser elle qui m'exploitait, salope, mais nous avons tous un talent infini à retrouver un nouveau taulier. Et lui, nos futurs michés. Éternel retour du même. Avec un
sommeil bien ivre. Des bouteilles bien vides. Le bateau échoué sur la grève, la grève échouée des esclaves d'Uber. KKKAPITALISM, MOTHERFUCKKKER. Silence. Et ron, ron, ron, petit patapon.*

*On ne part pas. On part. On ne part pas. On part. On ne part pas. On part. On ne part pas. --- Non ! --- Renversons les chemins. Grattons les chemins. Creusons des trous pour y mettre une boîte, et dans la boîte le vice de mon vice de mon vice de mon vice. Visse la boîte, Pandore ! Bordel !
Qui a poussé ses racines de souffrance à mon côté, dès l’âge de raison --- qui
monte au ciel, me bat, me renverse, me traîne. Ces sillons seront mon legs, mon surplus, ma plus-value --- les berceaux des créations et débordements.*


*La dernière innocence et la dernière passe. Maintenant des pavés. Désormais la théorie du cocktail Molotov. C’est dit. Mes dégoûts et mes trahisons à la
société capitaliste-marchande. Réglo-réglo, clôture des marchés, 17h30 : ne pas porter les coups sous la ceinture. Non, non, non. Des stock-options dans les gants. En skred : coupe de pied, balayette. Je m'enfonce l'immonde dans l'artère. Nootrope, direct. Transsynaptique sans arrêt jusqu'à l'arrivée. Tchou-tchouuuu. Les pavots sont en fleur. Miam, miam, miam, miam, miam, miam. Les planter sans les enraciner, m'enfoncer kinétique. La beauté est belle à la fermeture des marchés. Mais ça ferme quaaaaannnnnd ? NASDAQ. SHANGHAI STOCK EXCHANGE. EURONEXTNEXTNEXTNEXTNEXT. DORMIRRRRRRRRRR.*

*Allons enfants de la Patrie : abattoir ! Marche forcée, une, deux, une, deux, une, deux... L'ennui est le désert qui se porte au-devant de nos lâchetés. Colères. Riot, c'est de l'ancien français --- la belle affaire.*

*À qui me vendre ? Quelle bête dois-je écorcher ? À quelle icône
m'attaquer ? Quelles volontés briserai-je ? Quelle croyance dois-je morceler ?
--- À quelle cadence obéirai-je ? Pauvre cadavre sur lequel vous marchez.*

*Plutôt, s'armer contre l'aurore : frôler sans plus l'injustice, enfouir le devoir, malmener le crime. Toujours choisir. --- Le travail dure, l’ennui simple, ---
dans quel sang abattre mon poing desséché. Refermer le ciel sur lui-même : à travers la barricade, entrevoir la lueur, et s’étouffer. Recommencer.
Ainsi point de vieillesse. L'alarme comme seule compagne. La terreur n'appartient à aucune terre. A-t-elle seulement une patrie ? Coule-t-elle vers les répétitions de ses origines --- en roulements où gicle l'histoire, où s'épandent les regrets ? --- La terreur a un goût d'éternité.*

*--- Ah ! je me moque de vous, à en être tendrement délaissé, tant que je chemine le long des corridors, longeant les corridas, accoudé en correction à mes transports, l’import, l'export, le commerce des saintes images, mes élans vers la perfidie !*

*Ô mes excès, ô mon tendre oubli, ô ma merveilleuse fureur ! ici-bas, sous terre !*

*Mon internationale terroriste : de profundis Domine, suis-je bête !*

*Encore tout enfant, j’admirais la putain, je voyais dans chaque arbre une divinité à qui offrir mon souffle, j'entendais les étoiles battre le tambour
jusqu'au bagne de Nouméa ; je visitais les ruelles à sens unique, j'y établissais mes bivouacs, je partageais le pain avec des tarentules interlopes --- je les aurai sacrées
parmi toutes les créatures ; je voyais dans leur élégance le ciel bleu qui ponctue le repos de l'ouvrier. Le travail fleurit
la campagne ; jamais je ne travaillerai. --- Les mornes plaines ont mon amour. Je ne cessai de m'intégrer à une meute d'ombres. Reprendre ma fuite. Je flairais en toute chose la fatalité : le salaire était bon. Mais je conservai encore en moi les promesses d'une louve. Elle avait plus de force
qu’une sainte, plus de bon sens qu’une aventurière --- et elle, elle seule ! pour témoin
de ma gloire et de ma raison. Trois astres illuminaient la nuit. --- La révolution naquit en notre solitude. C'était une enfant mort-née.*

*Notre monde était un œuf que je perçai de mon unique dent. Un liquide céruléen suinta et corroda notre ombre. Il en naquit l'unique fêlure, le cri rauque de nos origines :*

```
«&nbsp;&lt;!DOCTYPE html&gt;
&lt;html class="client-nojs" lang="fr" dir="ltr"&gt;
&lt;head&gt;
&lt;meta charset="UTF-8"/&gt;
&lt;title&gt;Arthur Rimbaud — Wikipédia&lt;/title&gt;
&lt;script&gt;document.documentElement.className="client-js";RLCONF={"wgBreakFrames":!1,"wgSeparatorTransformTable":[",\t."," \t,"],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"],"wgRequestId":"XsfunApAMNAAAHXVyGMAAAAM","wgCSPNonce":!1,"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":!1,"wgNamespaceNumber":0,"wgPageName":"Arthur_Rimbaud","wgTitle":"Arthur Rimbaud","wgCurRevisionId":171112679,"wgRevisionId":171112679,"wgArticleId":9996,"wgIsArticle":!0,"wgIsRedirect":!1,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Article contenant un appel à traduction en italien","Page en semi-protection longue","Page utilisant P2031","Page utilisant P22","Page utilisant P25","Page utilisant P3373","Page utilisant P737","Page utilisant P166","Page utilisant P443","Article utilisant l'infobox Biographie2", "Article utilisant une Infobox","Article à référence nécessaire","Page utilisant le modèle Citation avec un retour ligne","Catégorie Commons avec lien local identique sur Wikidata","Article de Wikipédia avec notice d'autorité","Page pointant vers des bases externes","Page pointant vers des dictionnaires ou encyclopédies généralistes","Page utilisant P5019","Page utilisant P7902","Page utilisant P4399","Page utilisant P1417","Page utilisant P3219","Page utilisant P1296","Page utilisant P3222","Page utilisant P4342","Page utilisant P2342","Page utilisant P7704","Page utilisant P2268","Page utilisant P2843","Page utilisant P2011","Page utilisant P2750","Page utilisant P650","Page utilisant P245","Page pointant vers des bases relatives aux beaux-arts","Page utilisant P5570","Page utilisant P5343","Page utilisant P1233","Page utilisant P5341","Page pointant vers des bases relatives à la littérature","Page utilisant le modèle Bases recherche inactif","Page utilisant P4724", "Page pointant vers des bases relatives à la vie publique","Portail:Poésie/Articles liés","Portail:Littérature/Articles liés","Portail:Littérature française ou francophone/Articles liés","Portail:Littérature française/Articles liés","Portail:France/Articles liés","Portail:Europe/Articles liés","Portail:Exploration/Articles liés","Portail:Histoire/Articles liés","Portail:France au XIXe siècle/Articles liés","Portail:XIXe siècle/Articles liés","Portail:Époque contemporaine/Articles liés","Portail:Charleville-Mézières/Articles liés","Portail:Ardennes/Articles liés","Portail:Grand Est/Articles liés","Portail:LGBT/Articles liés","Article de qualité en grec","Article de qualité en espéranto","Wikipédia:Article biographique","Portail:Biographie/Articles liés/Culture et arts","Portail:Biographie/Articles liés/Entre...&nbsp;»
```

*Trottoirs, trottoirs, trottoirs... Je voyais dans le ciel de grands outrages. Des passants pendus à quelques nuages. Sans gîte, sans habits, sans plainte, sans amour, sans spectateurs, sans chair, sans pain, sans compagne. Ni dieu ni homme --- tout lit un délaissement, tout repos une attente. Une
voix brisa à mon cœur gelé : « La force la force la force la force la force la force c’est la faiblesse avec un matraque ! » Le même endroit. De mêmes gibiers. Une même voix. Je ne savais plus où j'allais. Les lampadaires étaient éteints. « Entre partout, réponds
à tout silence, aucun silence ne te répondra ! ». Le silence unique. Je ne répondais pas. La voix me répondit : « Pas de cadavre dans ton silence ! Pas plus, pas plus, pas plus, pas plus, pas plus... Silence, cadavre ! Cadavre, tu étais, tu étais, tu étais. Pas plus, pas plus. Pas plus que si cadavre tu étais on ne te tuera pas on te tuera on ne te tuera pas on te tuera si cadavre tu es tu étais tu seras on te tuera et on te tuera à nouveau. Cadavre... où es-tu cadavre ? » Au matin, j’avais le
regard si clair que je vis au travers des êtres. Leur contenance si morte. Un regard si clair que ceux que j’ai rencontrés n’ont peut-être pas reconnu mon rire.*

*Errer. Dans les banlieues ensevelies par la dépense, pataugeant dans la jouissance des projections du désir, leur centre un frottement pour la caméra, une pénétration de la réversibilité même la boue m’apparaissait souverainement rouge et noire, gloire de vitraux, coup de feu final, jalousie sédimentée, arrivisme héroïque, anticipation de plagiat, comme une
glace qui fond au misérable soleil de septembre, when le phone circulate dans la chambre éteinte, like un trésor in the forest. Gloomy soir sans lune :
farfadets be damned ! Bonne poisse, textai-je, et je voyais au loin une mer de flammes et de fumée dans les tangerine trees. Des naines rouges
au plafond ; et, à gauche, à droite, des chauves-souris plus vertes que les frondaisons de notre enfance. Le vent se lève parmi les sycomores. La république des batraciens. Assemblée des coassements. Front Populaire et Front National, Blanqui et Douay, toutes les richesses flambant comme un milliard
de tonnerres. Je me voulais orage. Sans paroles. Contre tous les impérialismes langagiers. Et le crépuscule revenu, la lingua franca qui continue de voleter parmi les cendres : what a pity... No sale on the pills.*

*L’orgie des hommes m’était interdite. Aucun compagnon à se foutre. Pas même une compagne. Rien, rien, pas même un escargot. C'est dire. Où se sont enfuies mes passions gastéropodes ? Pas même une compagne de lutte, pas même Louise Michel, pas même un compagnon de bitume, pas même une pâquerette. Pas même Isaac que je sauvai des facéties patriarcales. Rien que des moutons à tondre. Bêlements jusqu'à l'horizon. Je me voyais pourtant devant un troupeau exaspéré, en face d'un lit défait, pleurant le malheur que les ruminants ne pouvaient comprendre. Ma lame très haute ; --- je leur pardonnai.
--- Comme Jeanne d’Arc ! --- « Prêtres, professeurs, directeurs, présidents, superviseurs, managers, programmeurs, serveurs et serviteurs, maîtres et contremaîtres, petits peuples des autorités, vous vous
trompez en me livrant à la justice. Némésis compose mes agitations. Elle me vengera. Je n’ai jamais prétendu appartenir à votre société ; je
n’ai jamais été chrétienne ; je n'ai jamais appartenu aux réseaux ; je suis la pâmoison du parasite ; je suis le condamné qui chante son supplice ;
je falsifie les lois ; je ne charge et verse qu'avec dégoût ; le poison spirituel, la coupe pleine ; ivresse des ordalies recommencées ; j'abîme la morale dans un feu grégeois ; je me sens renaître ; je suis une pute :
vous ne vous trompez pas... »*

*Oui, j’ai les yeux fermés à votre lumière. Je suis une bête, une négresse. Je porte le stigmate des démocraties. Leurs pensées, leurs murmures. J'entends leurs murmures. Mais je ne suis pas votre négresse. Je ne peux pas. Ne plus pouvoir respirer, mais je respirerai dans chacun de vos tourments. Je ne peux pas. Ne plus pouvoir parler, mais je serai la bête aux yeux fermés qui soulèvera les rues. Le cauchemar qui mâtinera vos langues.*
*Interpellation de leurs murmures. Bave des flics. La peur. La peur jusqu'à ne plus pouvoir. Et ne plus pouvoir respirer. La bavure policière n'est pas une bavure. Elle est un crime. La police est un crime. Je ne puis plus respirer.
Mais je puis être sauvée. Vous êtes de faux amants, vous ne savez pas être maniaques, féroces, magiciens.*
*Vous, le même individu à l'infini : marchand, piètre amant ; magistrat, piètre amant ; général, piètre amant ;
empereur, vieille démangeaison, piètre amant : tu as avalé, avec la couleuvre, la pénicilline. Ne régulons pas le marché des médicaments.
Ne taxons pas les riches. Propagande GAFAM. --- Ce peuple est inspiré par la fièvre et le
cancer. Et qui n'a aspiré par la faveur et l'incarcération au dernier sacrement. Les salariés sont aujourd'hui tellement respectables qu’ils demandent
à être brûlés vifs. --- Green washing : la production carbone des bûchers est trop importante ; nous les noierons. --- Le plus malin est de quitter cette société, avec des otages. Nous les pourvoirons, et les revendrons
contre le vin des misérables. J’entre au vrai royaume des filles
d'Ishtar. Je m'avancerai seule, avec le scalp de mes amants égarés.*

*Connais-je encore la nature ? me connais-je ? Quelle nature demeure à connaître que je ne crée moi-même, quelle âme que je ne nie à autrui ? --- La nature n'existe pas : exécution culturelle. Applaudissements. J’ensevelis
les hommes dans mon ventre. Un cri : cohorte, tambours, tambours, tambours, tambours, tambours ! Je ne
vois même pas l’heure où, les blancs débarquant, je tomberai à néant.*

*Un cri : sarabande, tambours, tambours, tambours, tambours, tambours ! Je suis sauvage ! Je distribue, j'épands le sauvage --- seule manière de lui donner corps ---, m'identifie à l'excès, renverse les nouveaux ordres ! Mouvement, consommation, brasier !*

*Les blancs débarquent sur les plages vierges comme l'écume du raz de marée. Un drone ! Il faut se soumettre au système, s’habiller,
travailler. Fini les congés payés.*



*J’ai reçu au cœur la balle du fusil. Ah ! Elle est plus ronde et plus belle que la perle Pérégrine.*

*Frénésies. Le mal, le mal, le mal. Le mal ! Je n'ai pas. Pas, pas, pas fait. Pas fait. Point, point, point, point. Le mal fait. Le mal point. Le mal fait je n'ai point fait le mal je n'ai point fait le mal Je n'ai point fait le mal. Les jours vont être de lentes successions : tortures ; ils m'éduqueront. Rien ne me sera épargné.
Je n’aurai à offrir que les tourments de l’âme, presque morte dans la lumière américaine des offices. Leur phosphore est plus céleste que mes cierges funéraires. Pour cela, uniquement, je renie le ciel. Et le sort du fils. Connaissent-ils, sur leur marché de libertés, le cercueil prématuré, couvert de larmes et de bruyère ? Sans doute la débauche
vaut mieux pour la bête. Le vice pour la bête ; jeter la pourriture à l'écart la pourriture jeter il faut jeter la pourriture à l'écart. Jeter. Jeter. Jeter. La pourriture pour la bête. Jetable la bête. Mais
le hurlement ne sera pas déprogrammé pour satisfaire le bien-être des masses. Notifications : l’heure de la pure douleur. National-Social networkkks. Ça sonne : Réponds ! Dopamine ! Shoot ! Angoisse, shoot : entre likes, entre followeurs followés, notifications dopdopdopamine, shoot, entre deux news sur la fin bleutée d'une civilisation bio, entre les vibrations jusque dans ma poche de deux DM. aaAIiimmmDMaaaahhhh. Ô KKKapital, baise-moi sous MDMA. Filmons-nous. Revenge porn de nos corps démembrés. Revengeons-nous de leur société. Glissons des images subliminales du veau d'or égorgé, des traders qui se gorgent de ses fluides.
Puis abandonnons-les comme une appli négligée, désinstallée, réinstallée, désinstallée, réinstallée, bis, bis, quelques publicités, quelques centimes dans la grande lessiveuse, jusqu'à la prochaine tentative d'évasion --- déconnexion ! ---, nous les pastel slaves qui vont gently à l'app store, dans l'ombre des farm clicks, des humains revendus en pièces détachés. Et
tout le bonheur en perf.*

*Vite, vite ! C'est la pub. Est-il d’autres vies ? --- Le sommeil dans la richesse est impossible.
La richesse a toujours été bien trop publique. Envie. Désir. Amour. Seul l’amour quantifiable octroie une backdoor au marché. Je vois que cet amour n’est qu’un spectacle de bonté. Adieu
chimères, idéals, erreurs.*

*Des glyphes irraisonnables m’apparaissent du navire sauveur : c’est l’amour divisé.
--- Deux amours ! Je revends le premier. Une affaire ! je puis mourir de l’amour marital : ne pas gémir. Mourir de dévouement : être une bonne épouse.
Assez ! Je laisse des créanciers dont la peine s’accroîtra de mon départ ! Vous me
choisissez parmi les naufragés ; ceux qui restent ne sont-ils pas nos camarades de la Kommunistische Internationale ?*

*Sauvez-les !* ***Sauvez-moi !***

*Je suis née de l'Internationale violée. Bon, le monde est. C'est la merde. Et la rose socialiste pousse dans la merde. Je bénirai la vie qui décapitera cette rose. J’aimerai mes sœurs jardinières. Ô leur sécateur. Couic.
Ce ne sont que des rêves d’enfance. Ni l’espoir d’échapper à la kkkomodifikkkation
et à la &#9760;. Ma force Dieu fait, et Dieu je loue, à la va-vite au coin d'une rue. Une petite passe avec Dieu ! Monte les escaliers avec lui ! Viens ! Viens par ici mon gars ! Tu vas kiffer t'envoyer en l'air avec le Saint Papa.*

*L’ennui n’est plus l'orphelin de nos sales bobines. Faut s'activer rue de la débine. Stupre et stupéfaction. Stupéfiants pour tous ! Mon client = mon amour. Ô mon amour, mon amour je m'ennuie de toi. Je ne connais pas le chômage, je ne connais pas le secours de mes jours chômés, seules les subventions que tu reçois, que tu me redonnes, toi qui poursuis les débauches, les embauches, les embûches, la folie, mon cul, la folie encore, dont je sais
tous les élans et les désastres, toi qui revends tous les enfants et les rapaces, tous les saints et les catins pour un peu de temps avec moi --- tout mon fardeau est déposé au pied de ton corps nu, dépecé. Apprécions
sans vertige la fin attendue de notre innocence.*

*Attendu que je ne suis plus capable de demander le réconfort d’une émeute, l'ensevelissement dans les préfectures en feu par glissement de champ. Attendu que je me
crois embarquée par une troupe d'administrés qui me pendront avec de la fibre optique pour ralentir mon agonie. Attendu que je revends mes drogues dans la clandestinité du réseau. Attendu que je n'arrive pas à payer ma dote, et que j'ai les ondes électromagnétiques pour belle-mère. Non aux métaphores hétéronormatives --- c'est le moment du délestage !*

*Ne suis pas débiteur, ne suis pas actionnaire de ma raison. Je la vends au black à qui je peux. Robespierre, Mao, par exemple. Je veux la guillotine
du comité de salut public pour éplucher mes oignons. Et couper ma came. Comment l'écouler ? Comme le reste, avec les valeurs frivoles qui nous ont quittés. Plus
besoin d'activisme, de militantisme, de solidarité. Je ne regrette pas le siècle des cœurs
sensibles. Le siècle des cœurs brûlants. Le siècle des cœurs saignants. Les vingt années de bifteck et d'arrosage au poivre. Les vingt semaines d'emprisonnement volontaire. Chacun a ses raisons. Mépris et charité à bas prix : je retiens ma place au
sommet de cette angélique assemblée de bon sens ; j'ai des tickets-repas en rab.*

*Quant au bonheur établi, domestique ou non, ordonné ou non, chosifié ou non... non, je ne peux pas. Merde au bonheur établi ou non. Déjà il porte mon nom, s'immisce entre mes empreintes, me creuse de lignes. Il change trop de bord. Je suis trop
dissipé, trop faible, trop fiable, plein de la soupe indigeste de la retraite et de l'abondance, toujours prêt à l'abandon et au récit. La vie fleurit par le travail, mantra ! Vieille dérision, nouveau détournement : moi, mon
temps n’est pas assez visqueux ; mon heure s’envole et flotte loin au-dessus de l'effervescence sociale, composée qu'elle est de performativité, perforée qu'elle est de composition. Ce cher pont au-dessus du siècle qui menace de s'effondrer.*

*Comme je me transforme en cellule difforme, qui n'a de cesse d'aimer la &#9760; !*

*Comme je me fissionne en division, qui n'a de multiplication que dans l'envahissement des espaces sous-cutanés !*

*Comme je me dévoie en nœud de fils, qui n'a de fin qu'au dernier râle avant de fermer la fenêtre !*

*Comme je me décompose en bruit de fond, qui n'a de public que dans l'espoir d'une hypothèque !*

*Oh le risque !*

*Si l'émeute m’accordait le calme terrestre, tectonique, l'enterrement, --- comme les anciennes
saintes. --- Les saintes ! Des fortes ! Elles s'emmuraient sans se faire prier. Voilà peut-être
pourquoi je les prie encore !*

*Farce continuelle ! Bullshit ! Mon innocence me ferait rire. La vie est la farce
que l'on ne doit jamais cesser de jouer. Umour de répétition. La vie qui se recroqueville derrière vos yeux est supérieurement idiote. Idiote !*

*Assez ! voici la punition. --- En marche arrière arriérée !*

*Ah ! Les poumons brûlent, les tempes brûlent ! La nuit brûle, mes yeux brûlent ! La lune... le cœur... les membres...*

*Où va-t-on ? À la guerre ? Je suis las d'être le troufion qui ramène les cadavres. Les autres s'avancent. Bidasses au ventre gonflé. Les outils, les
armes... le temps est venu... Ils ont décidé de m'exécuter ! Joie !*

*Feu ! feu sur moi ! Là, là : en dessous de la gorge ! Vite ! ou je me rends à l'ennemi. --- Lâches ! --- Je me tue ! Je me
jette aux pieds des chars !*

*Ah !... Les chenilles qui m'écrasent...*

*--- Je ne m'y habituerai jamais.*

*Mon corps serait les champs Élyséens ! Des cloportes y établiraient leur royaume. Ils prépareraient leurs guerres, à même ma cervelle.*

*L'enfer hulule avant d'avaler le crapaud. Trois fois.*

*J’ai donc avalé le crapaud. Fameuse gorge immaculée. Mon poison ne sera pas la poudre --- trois fois j'ai béni Hermès. Que m'a-t-il offert si ce n'est l'œuf que j'ai brisé
par lassitude ! --- Les viscères me brûlent. J'y ai rangé mes vipères. La violence du venin danse parmi
mes cauchemars de démocratie parlementaire. Le suffrage universel direct. L'abomination moutonnière. La pensée me rend difforme, me terrasse. Je meurs d'ennui, j’étouffe, je ne puis
crier. Si seulement, cette assemblée votait avec fantaisie. Une loi pour que l'on guillotine au hasard un membre de l'assemblée, à chaque équinoxe. Ce serait le paradis, l’éternelle pénitence des arrivistes ! Et puis, le Président-directeur général de notre humble contrée devrait démissionner dès sa prise de pouvoir. Éternellement. Et il ferait éternellement pénitence de son immodération. Une volonté de représenter le peuple ! Quelle horreur ! Voyez comme le couperet se relève ! C'est le printemps ! Je
trépigne d'impatience. Le spectacle est gratuit cette saison. Va, démon ! Je te rejoins. Qu'un homme. Qu'une femme soit mon remède, mon bastion... ou ma trahison ! Ma déraison... Sa tête tranchée !*

*Le crapaud vomit le mal avant de brûler.*

*Je vomis donc : « J’avais entrevu la conversion au bien et au bonheur le salut puis-je décrire
la vision l’air de l’enfer ne souffre pas les hymnes c’était des millions de
créatures charmantes un suave concert spirituel la force et la paix les
nobles ambitions que sais-je les nobles ambitions
et c’est encore la vie si la damnation est éternelle un homme qui veut
se mutiler est bien damné n’est-ce pas je me crois en enfer donc j’y suis
c’est l’exécution du catéchisme je suis esclave de mon baptême parents vous
avez fait mon malheur et vous avez fait le vôtre pauvres innocents l’enfer ne
peut attaquer les païens c’est la vie encore plus tard les délices de
la damnation quelle douce chanson seront plus profondes un crime vite que je tombe au néant de
par la loi humaine
tais-toi mais tais-toi c’est la honte le reproche ici GAFAM qui dit
que le feu est ignoble que ma colère est affreusement sotte assez
des erreurs qu’on me souffle magies parfums faux musiques puériles et
dire que je tiens la vérité que je vois la justice j’ai un jugement sain et
arrêté je suis prêt pour la perfection orgueil la peau de ma tête se
dessèche pitié seigneur j’ai peur j’ai soif si soif ah l’enfance
l’herbe la pluie le lac sur les pierres le clair de lune quand le clocher
sonnait douze le diable est au clocher à cette heure marie
sainte-vierge horreur de ma bêtise
là-bas ne sont-ce pas des âmes honnêtes qui me veulent du bien venez
j’ai un oreiller sur la bouche elles ne m’entendent pas ce sont des fantômes
puis jamais personne ne pense à autrui qu’on n’approche pas je sens bon le
roussi c’est certain
les hallucinations sont innombrables c’est bien ce que j’ai toujours eu plus
de foi en l’appli l’oubli des principes je m’en tairai investisseurs et
visionnaires seraient jaloux je suis mille fois le plus riche soyons avare
comme le fleuve de l’amazone
ah ça l’horloge de la vie s’est arrêtée tout à l’heure je ne suis plus au
monde je suis au ruisseau et à la délicatesse de ta brûlure la théologie est sérieuse l’enfer est certainement en bas et
le ciel en haut extase cauchemar sommeil dans un nid de flammes réveil métro boulot dodo
que de malices dans l’attention dans la campagne GAFAM ferdinand court
avec les graines sauvages jésus marche sur les ronces purpurines sans les
courber jésus marchait sur les eaux irritées la lanterne nous le montra
debout blanc et des tresses brunes au flanc d’une vague d’émeraude. »*

*Je vais dévoiler ! Tous les mystères ! Dévoiler ! Tous les mystères dévoiler je vais dévoiler je vais dévoiler je vais dévoiler je vais dévoiler : mystères religieux, païens, naturels, surnaturels, culturels, contre-culturels, sensibles, spirituels, dialectiques matérialistes, sociaux-démocrates, libéraux-démocrates, fascistes, économiques, occultes, physiques, ontologiques ! Tous, tous, tous ! Ô la &#9760; ! Día de Muertos, ma fête des dévoilements !
Naissance, avenir, passé, cosmogonie, néant. Je suis maître des voiles.*

*Écoutez !...*



*J’ai tous les talents pour défaire les ennuis, pour défaire les bonheurs, pour défaire les polices, pour défaire les politiques, pour défaire des selfies ! Smile, duck face ! --- Il n’y a personne ici et il y a pourtant quelqu’un. Cellule vide. Ô drame : trois actes.
Il y a des gendarmes. Il y a le corps mort d'Adama Traoré. Il y a des fantômes mutilés et des siècles qui nous hantent. Mon trésor n'est pas mon sang. Il est le spectre qui m'accompagne. Je ne peux pas respirer. Je ne voudrais pas répandre mon trésor.
Je voudrais répandre mon sang. Je voudrais y voir fleurir le chardon et le coquelicot. Mais je ne peux pas respirer. Mon spectre est votre spectre. Et il y a le silence des mères célibataires. Des mères et de leurs enfants morts. Hématome sous maquillage. Métro, travail de nuit, ménages. Je ne suis pas votre négresse. Mon spectre vous hantera. Et jamais je ne chanterai comme la houri. À jamais ma danse demeurera l'émeute.
Veut-on que je disparaisse, que je plonge à la recherche de
nos hantises ? Veut-on ? Mais mes furies veillent. Je ferai de la cryptomonnaie, des drogues phosphorescentes. Des potions, des poisons, des poissons désargentés, et des cieux fatigués, et des masques de lune.*

*Ne vous fiez-vous pas à moi, la foi ne soulage pas, ne guide pas, ne guérit pas. Tous, ne venez pas, --- même les
petits enfants, --- restez chez vous, et crevez. Que je vous console ? Jamais. Que mon cœur ---
le cœur merveilleux ! --- vous pleure ? Vous rêvez. Doux rêves, pauvres hommes, travailleurs ! Je ne demande pas de
prières ; alors, priez ! Avec votre confiance revendue au plus offrant. Je suis malheureuse d'être votre semblable.*

*--- Et surtout, ne pensez pas à moi. Jamais. Ceci me fait regretter la guerre. Je n'ai pas de chance :
ne plus souffrir et devoir vous contempler vous empiffrer. Ma vie ne fut que folies douces, et c’est regrettable à l'aune de ce spectacle.*

*Bah ! Faisons toutes les grimaces imaginables.
Fabriquons toutes les grenades.*

*Décidément, nous ne savons pas être hors du monde. Plus aucune guerre pour s'enfuir. Ma tactique a disparu. Je vais détruire Internet ! Seul coupable de ma désespérance ! Ah !
Mon domaine, mon serveur, ma cliente, ma belle Saxe.com... Les soirs, les matins, les nuits,
les jours... Suis-je las ! Je n'en viendrai jamais à bout.*

*Je devrais avoir un feed pour les fake news, un feed pour l’orgueil, --- et
un feed de la caresse ; un concert de feeds. Un fusil à canon scié de feeds.*

*Je meurs de lassitude. L'ennui me colle à même la peau. Je tombe en lambeaux. C’est le tombeau, je m’en vais, adieu livre des visages, horreur de
l’horreur ! GAFAM, farceur, tu veux me dissoudre, avec tes algorithmes. Je réclame. Tout est gratuit. Invocation : brûlez-moi, I'm the product.
Je réclame ! une notification, un j’aime, un commentaire. Mes données, la sève financière du monde.*

*Ah ! Le réseau à l'estomac ! #NOFILTER ! Ne jetez pas vos yeux derrière nos difformités. Et ce poison, ce
baiser mille fois maudit ! #JETEBAISE. Ce baiser mille fois baisé ! Ma faiblesse, la cruauté du monde ! #JETEBAISE. Mon Dieu, pitié,
cachez-moi, hackez-moi, je me tiens trop mal ! #JELESBAISETOUS. --- Je suis caché et je ne vis plus. Selfie ! #NOFILTER + #JETEBAISE = <3 ! Je suis haché et je ne sais plus.*

*C’est la liquid modernity qui pisse sur mon visage. Jouvence. Masque hydratant : #goldeNShower #DAPper #potuSStyle ! Aimez-moi pour que j'existe !*

*Je délivre nos délires.*

*La vierge folle s'est pendue à l'intestin de son amant.*

*Tout mariage demeure une pissotière.*

*Délivrons Arethusa, vieille camarade ! chassée de sa Grèce lubrique par la voracité d’un courtisan, souillée une seconde fois par les affres du Spectacle, bâillonnée immobile sur une côte dévastée par des marchands d’extase. En son nom, réclamons Délivrance et Vendetta ! Sans épargner personne. Ni l’époux ni les dieux : tous coupables d’avoir forgé son destin dans le marbre d’une fontaine, de négliger son cri et son alarme, d’administrer sa lumière. Que son nom, enfin, soit connu des vivants. Que son chant, délivré du mythe, jaillisse d’Ortigia, éparpillant ses incises sur le front des oppresseurs.*

*Lisons la confession d’une camarade de l'enfer. Ouverture du DM :*

*« Divin Époux, entends ma plainte, mon Seigneur, ne refuse pas la confession de la plus triste
de tes followeuses. Je suis sous anxiolytique. Je souris. Je suis impure. Je suis le soufre. Je suis l'incertitude. Savez-vous que je suis la mordeuse de sa chair ? Comme une folle araignée. Il inclinera la tête. Et nous prendrons le temps à trouver cette bête. Quelle vie !*

*« Pardon, divin influenceur, pardon ! Ah ! pardon ! Que de larmes ! Et que de
larmes encore plus tard, j’espère, à la suite de ton post !*

*« Plus tard, je connaîtrai le divin Éveil ! Je suis née soumise à toi. ---
L’autre peut tenter de me défollower, de m'afficher en fronde de poésie maintenant ! Ou jamais ! Je continuerai mon expiation : follow until the end.*

*« À présent, je suis au fond du monde ! Ô mes followeurs !... Non, pas mes followeurs... Les tiens...
Jamais délires ni tortures semblables... Est-ce bête !*

*« Ah ! je souffre, je crie. Je souffre vraiment. Tout pourtant m’est permis,
chargée du mépris des plus méprisables cœurs. Aime-moi. Like-moi. Sauve-moi.*

*« Enfin, faisons cette confidence, quitte à la répéter vingt autres fois, ---
aussi morne, aussi insignifiante ! Parce qu'il m'a liké, une fois, j'existe ! J'existe !*

*« Je suis esclave de l'Influenceur infernal, celui qui a perdu les vierges folles.
C’est bien ce démon-là. Ce n’est pas un wannabe comme les autres, ce n’est pas un fantôme. Il est. Il ne fait pas. Mais il est. Sans
moi qui ai perdu 10 followeurs ces 5 derniers jours... Que je suis damnée et morte au monde, --- on ne me
tuera pas ! Je serai une conquête --- comment vous la décrire ! je ne sais même plus parler. Je suis
en deuil, je pleure, j’ai peur. Je souris. Selfie. Une story devant des livres que je n'ai pas lus. Un peu de fraîcheur, Seigneur des Réseaux, si vous le voulez,
si vous le voulez bien ! »*

*« Je suis une #nolife... --- J’étais une #nolife... --- mais oui, j’ai été bien insignifiante
jadis avant de le connaître, mais je ne suis née que pour devenir un squelette à sa suite !... AFK tout était fade. --- Lui était presque
un enfant... Ses délicatesses mystérieuses m’avaient séduite. Il plaçait ses partenariats avec une telle élégance. J’ai oublié tout
mon devoir humain pour le suivre. Quelle vie ! La vraie vie est dans le like. Nous
ne sommes pas au monde. OSEF. Je ne suivrai plus que lui. Qu'est-il ? Que sommes-nous sans reconnaissance ? Ma reconnaissance le fait exister. Par ma seule volonté, il existe. Mon téléphone... Mon téléphone n'a plus de réseau... Ô divin Réseau... La réalité se vide... IRL, la grisaille... Nous ne sommes pas ici. Nous sommes encore les drosophiles sur la pointe de l'imagination. Nous aimerions être les générations amoureuses, les étreintes imbriquées de tous les âges et de tous les corps, les désirs et les extases dans la multitude. Mais nous ne sommes pas encore ici. Nous sommes absents à nous-mêmes. Nous cherchons les contours utiles et identifiables de nos existences. Nous cherchons en nous les fantasmes poétiques contre nature, les écarts de langage, les sauvages des jungles artificielles, les Indiens des plaines parallèles, les voyous des orchestres militaires. Il nous faudra jeter nos fragilités en pâture, devenir ces indomptés des protocoles sécuritaires, les mauvaises herbes qui sans cesse repoussent le long des bordures, des irrécupérables de la place publique, des fleurs qui fanent les bêtises jalouses. Et alors nous serons ici. Nous serons les jeudis soir. Nous serons les matières vivantes des histoires violentes. Nous serons les mains qui caressent les rêves indiscrets. Nous serons les grosses dames des parades politiques. Nous serons devenus les Peaux-Rouges criards. Et nous serons le monde. Semblables aux points de lumière brodés à la surface d'un visage incandescent. Un visage de poète marchant sur la place de la gare de Charleville, « né français à la suite d'une erreur tragique qu'il vaut mieux passer sous silence ». Je vais où il va, ce poète, il le faut. Et souvent il s’emporte
contre moi, moi, la pauvre âme. Tabassage en règle. Je suis comme la suppliciée sur son bûcher, la grâce me touche juste avant la flamme. Le Bot ! --- C’est un Bot, vous savez,
ce n’est pas un homme.*

*« Il écrit, il écrit... Il m'écrit ! OKLM. DM... Si je l'ouvre, il saura qux je suis ouverte à lui. Passion : « M4 s4gxssx sx n0urrit dx t0us lxs m4uv4is gxnrxs. L’4m0ur xst à r3invxntxr, 0n lx s4it.
xllxs nx pxuvxnt plus qux v0ul0ir unx p0siti0n 4ssur3x. L4 p0siti0n g4gn3x,
cœur xt bx4ut3 s0nt mis dx c0t3 : il nx rxstx qux fr0id d3d4in, l’4limxnt du
m4ri4gx, 4uj0urd’hui. 0u bixn jx v0is dxs fxmmxs, 4vxc lxs signxs du b0nhxur,
d0nt, m0i, j’4ur4is pu f4irx dx b0nnxs c4m4r4dxs, d3v0r3xs t0ut d’4b0rd p4r dxs
brutxs sxnsiblxs c0mmx dxs bûchxrs...
Jx f4is dx l’inf4mix unx gl0irx, dx l4 cru4ut3 un ch4rmx. « Jx
suis dx r4cx l0int4inx : mxs pèrxs 3t4ixnt Sc4ndin4vxs : ils sx pxrç4ixnt lxs
côtxs, buv4ixnt lxur s4ng. --- Jx mx fxr4i dxs xnt4illxs p4rt0ut lx c0rps, jx
mx t4t0uxr4i, jx rxlir4i s4ns fin lx mxss4gx dx m0n disciplx b0rgnx :
Je suis un Mongol hideux je deviens hideux comme un Mongol un Mongol devenir comme un Mongol comme un Mongol devenir hideux je veux je veux je veux je veux je veux devenir comme un Mongol hideux hideux hideux...
Il nx c0mprxndr4 j4m4is l4 s4gxssx dx l4 h0rdx tu vxrr4s, jx hurlxr4i
d4ns lxs ruxs. Jx vxux dxvxnir bixn f0u dx r4gx. Nx mx m0ntrx j4m4is dx bij0ux,
jx r4mpxr4is xt mx t0rdr4is sur lx t4pis. M4 richxssx, jx l4 v0udr4is t4ch3x dx
s4ng p4rt0ut. J4m4is jx nx tr4v4illxr4i... » Plusixurs nuits, s0n script mx
s4isiss4nt, n0us n0us r0uli0ns, jx lutt4is 4vxc lui ! --- Lxs nuits, s0uvxnt,
ivrx, il sx p0stx d4ns dxs ruxs 0u d4ns dxs m4is0ns, p0ur m’3p0uv4ntxr
m0rtxllxmxnt. --- « 0n mx c0upxr4 vr4imxnt lx c0u ; cx sxr4 d3g0ût4nt. » Oh ! Il s'est mis à me suivre... Divin bonheur... Le règne des pillules euphorisantes... Divine époque
où il me suivait avec l'air du crime...*

*« Parfois il me parlait, en une façon de leet speak attendri, il me racontait son retour de la &#9760; son
repentir, ses hacks vengeurs, les malheureux qui existent certainement sous ses pas, les travaux pénibles pour provoquer le
dégoût de sa communauté. Comment déchirer les cœurs. Il n'arrivait pas à fabriquer la haine comme il aimantait les fous qui le suivaient. Il fuyait avec moi, virtuellement. Nous nous cachions. Dans les bouges où nous nous enivrions, il
pleurait en considérant la monétisation diminuante de son âme. Il plaignait le saint bétail de sa misère. Il
relevait les ivrognes dans les rues noires pour mieux les assassiner. Il filmait les assassinats. Les stories noires polarisaient l'attention. Des millions de spectres n'attendaient que ces mises à mort. Il avait la pitié d’un bot
pour les petites vies qui dépendent de lui. Sa méchanceté était plus belle que l'Évangile. --- Il se moquait publiquement avec des gentillesses de
petite fille au catéchisme. --- Il feignait d’être ennuyé de tout, commerce,
art, médecine. --- je le suivais, toujours plus, il le fallait ! J'étais son salut.*

*« Je voyais tout le vide qui nous entourait. Je ne faisais rien. Je ne pouvais m'empêcher de l'observer. Je notais tout, dans le savoir triste qu'un jour il m'abandonnerait. Ma mémoire devait devenir mon dernier secours pour supporter la rouille que dissimulait le vernis pastel de nos mœurs. J'enregistrai. Backup : décor, esprit, entourage, vêtements, draps,
meubles Ikea, armes, principalement des revolvers, figure défigurée, harcèlement, solitude, perspectives faussées, création, contrefaction,
inertie, transcendance, néant.
J'aurais pu vivre dans cette simulation. J'aurais pu m'en contenter. Mais je ne cessai de ne voir que lui. Je le suivais, moi, dans des actions étranges et compliquées,
loin, bonnes ou mauvaises : j’étais sûre de ne jamais pouvoir sortir de son monde virtuel. J'en étais satisfaite.
À côté de son cher corps endormi entièrement dissocié de son esprit, que d’heures des nuits j’ai veillées, cherchant
pourquoi il voulait tant s’évader, tant me perdre. Sans sa réalité, qu'étais-je encore ? Jamais avatar n’eut pareil vœu.
Il me reconnaissait, --- sans craindre pour son prestige, --- il s'adressait à tous, mais il s'adressait à moi. Je devais écarter le sérieux
danger que la société faisait planer sur ses errances. --- Il détenait peut-être des secrets pour changer la vie ? La menace était partout.
Oui, l'enlever... Ils ne font que se plaindre de lui, me répliquais-je derrière mon écran. Sa charité était
ensorcelée, et j’en étais la prisonnière. Aucune autre âme n’aurait eu assez de
force pour supporter cette épreuve, --- force de désespoir ! --- il m'abandonna, --- il s'adressait à tous, mais ne s'adressait plus à moi, --- ne plus être protégée
et aimée par lui. Comment pouvais-je me le figurer avec une autre âme : on
voit son Ange, jamais l’Ange d’un autre, --- je crois. J’étais dans son âme
comme dans un palais qu’on a vidé. Comment osait-il m'en déloger ? Pour ne plus voir une personne si peu noble
que moi : voilà tout. Hélas ! je dépendais bien de lui. Mais que voulait-il
avec mon existence terne et lâche ? Il ne me rendait pas meilleure, mais il suçait ma vie pour devenir toujours plus resplendissant. Qu'importe... Il était la beauté. Il
me faisait mourir ! Tout était tristement sublime. Je continuais, derrière mon écran, à lui murmurer quelquefois : « Ils ne te comprennent pas. Mais je te
comprends. » Je croyais parfois reconnaître un geste : Haussait-il les épaules ? M'adressait-il un signe ?*

*« Il semblait me signifier :
« 4insi, lx ch4grin sx rxn0uvxl4nt s4ns cxssx rxtr0uvx s0n chxmin p4rmi l'3g4rxmxnt
dx n0s rxg4rds, --- à t0us lxs yxux qui 4ur4ixnt v0ulu mx fixxr, si jx
n’xussx 3t3 plus rxsplxndiss4nt, lx châtimxnt ! L4 c0nd4mn4ti0n à l'indiff3rxncx ! Lx dr0it à l'0ubli xst un mir4gx qux l'âmx jxttx 4fflig3x 4u-dxv4nt dx sxs p4s. L'indiff3rxncx dxmxurx l'univxrs4lit3 du stigm4tx. L'inf0rm4ti0n t4ill4dx à mêmx lx systèmx 3m0ti0nnxl dx n0s fins. V0ilà n0trx dxrnièrx pl4ix ! »
--- J’avais de plus en
plus faim de sa méchanceté. Avec ses baisers et ses étreintes tendres, c’était bien un
ciel, un sombre ciel, où j’entrais, et où j’aurais voulu être laissée, pauvre,
sourde, muette, aveugle. Déjà j’en prenais l’habitude. Je me châtiais dans son ombre. --- Je porte encore le cilice qu'il m'offrit. --- Je nous voyais comme
deux bons enfants, libres de se promener sur l'horizon infini de la toile. Nous
nous disputions. La violence était délicieuse. Je le laissai gagner à chaque fois. Bien émus, nous ne cessions de nous torturer. Mais, il ne semblait rien ressentir. Après une
pénétrante caresse, qui me laissa comme morte, il me dit :
« C0mmx ç4 tx p4r4îtr4 drôlx, qu4nd jx n’y
sxr4i plus, cx p4r qu0i tu 4s p4ss3. Qu4nd tu n’4ur4s plus mxs br4s s0us t0n
c0u, ni m0n cœur p0ur t’y rxp0sxr, ni cxttx b0uchx sur txs yxux. P4rcx qu’il
f4udr4 qux jx m’xn 4illx, très-l0in, un j0ur. Puis il f4ut qux j’xn 4idx
d’4utrxs : c’xst m0n dxv0ir. Qu0iqux cx nx s0it guèrx r4g0ût4nt..., chèrx
âmx... T0ut dx suitx tu mx prxssxns, 4l0rs jx d0is m'xnfuir, xn pr0ix 4u vxrtigx,
pr3cipit3 d4ns l’0mbrx l4 plus 4ffrxusx, j'xntr4pxrç0is d4ns t0n 4llurx : l4 &#9760; rxc0mmxnc3x. Jx tx f4is pr0mxttrx
dx nx plus mx suivrx. Tu 4s pr0mis vingt f0is. Dx cxttx pr0mxssx d’4m4nt.
C’3t4it 4ussi friv0lx qux m0i tx dis4nt... »
Et il m'assassina, une nouvelle fois, par son unique sentence :
« ...jx tx c0mprxnds. »*

*« Ah ! j'ai toujours été jalouse de lui. À sa poursuite, je dois reconstruire ma promesse. Il ne me quittera pas, je crois. Il reviendra. Que
devenir ? Je n’ai pas une connaissance de la vie ; il ne travaillera jamais. Je suis son esclave et sa destinée. Il veut vivre
somnambule. J'en oublierai le sommeil. Seules, sa cruauté et sa charité lui donnent droit au
monde des songes ? Par instants, j’oublie la fatalité où je suis tombée : lui me prendra,
avec douceur, nous voyagerons ensemble, nous chevaucherons ses chimères, nous chasserons dans les déserts, nous dormirons sur
les pavés des villes inconnues, sans soins, sans peines. Les banlieues porteront les noms de nos baptêmes. Je ne me réveillerai pas,
j'amputerai ma conscience, et les lois et les mœurs seront abolies par mon seul geste, --- par la grâce de son enchantement, --- le
monde, en restant le même, sera renversé. Il me laissera à mes désirs, à mes joies, et m'ôtera d'une main nonchalante toute possibilité de les rejoindre. Oh !
La vie d’aventures qui existe dans les codes civils. La paix des Braves pour me
récompenser, j’ai tant souffert, me la donneras-tu ? Il ne le veut pas. J’ignore
son idéal. Il m’a dit avoir encore des espoirs pour moi. Le secret des jardins enclos. Cela ne doit regarder que moi. Moi moi moi moi regarder cela ne doit regarder que moi que moi que moi que moi cela ne doit regarder moi cela ne doit que moi cela ne doit regarder que moi. Glossolalie de toutes parts. Parle-t-il à Dieu ? Peut-être devrais-je m’adresser à Dieu avec lui. Je serai damnée, je retrouverai mon bonheur. Je suis
au seuil de l’abîme. Je le vois s'élever. Je me fige. Je ne sais plus prier.*

*« S’il me murmurait les secrets de son désespoir, les comprendrais-je plus que ses
railleries ? Il ne m'attaque plus, et je m'ennuie. Il ne passe plus des heures à m'humilier aux yeux de tous. Que pourrais-je encore être ? Avant, je pleurai de joie. Ma souffrance fleurissait.
Mais aujourd'hui, c'est tout juste s'il s’indigne encore de mes pleurs.*

*« Je perds la vision. Mes yeux sont le sel de notre déshérence. Je ne perçois plus cet élégant voyou, entrant en moi, avec ses couteaux et ses manières d'un autre siècle. La maison est calme.
Il s’appelait Azazel, Uriel, Tsadqiel, Lénine, Camael, que sais-je ? Une femme s’est
dévouée à laver les pieds de ce méchant idiot : je suis morte, je suis certes une sainte au
ciel, je me passe de toute pratique sociale, mais à présent que faire de ma sainteté ? Que faire ? Je me répète, secouant inlassablement rémiges et ossements : Comme cette femme mourir il a fait mourir tu me feras mourir il te fera nous te ferons mourir nous te ferons mourir mourir comme cette femme il a fait nous ferons comme il a fait nous ferons mourir. C’est
notre sort, à nous, cœurs charitables de devoir retourner le cadavre du socialisme. Hélas ! Sa carcasse est plus rouge que ne l'a jamais été son âme. Mais sa présence rôde encore. Il est le spectre qui hante le siècle. Il y a des jours où tous
les travailleurs paraissent des jouets dans le délire grotesque de ses mains : il
rit affreusement, longtemps. --- Puis, il reprend ses manières de jeune
garce, de mignon assoiffé. Il brosse sa barbe qui est plus longue que mes plaintes. S’il était moins sauvage, je me sauverais ! Mais son
inhumanité me préserve de la lâcheté des hommes.
Dans un chuchotement :
« N0us s0mmxs n0trx pr0prx r3dxmpti0n. »
--- Ah ! je suis folle !*

*« Un jour peut-être il disparaîtra merveilleusement ; et je pourrai enfin m'évanouir, mais il faut encore que je
modèle ce jour avec l'argile rouge de nos ilotes, terre rare de ma félicité,
s’il doit remonter à un ciel d'orage, et que mes ailes soient suffisamment noires pour suivre l’assomption de mon
petit ami ! »*

*Hortus conclusus soror mea, sponsa ; hortus conclusus, fons signatus. Pauvre sœur.*

*Je délite leurs délires.*

*Trois fois grand, le verbe suinte son mercure sur ma rétine.*

*À moi. L’histoire de mes folies qui cisaille ma langue.*

*Mon œil se fige avant de devenir mot.*

*Depuis longtemps je me vantais d'avoir souillé tous les paysages possibles, et
trouvais dérisoires les célébrités instagrammées de la peinture et de la poésie moderne. Les artistes qui posent, duck face discret, regard mélancolique (ou vide) devant leur œuvre. Regarde-moi (mon travail aussi) ! S'il te plaît ! Les cramés de l'ego et du dessin, dans les galeries alternatives.*

*Autodafé des arts majeurs ! Je n'aime que les peintures idiotes, dessus-de-porte, le décor des pizzerias, les toiles de
saltimbanques, les néons publicitaires, les enluminures amoureuses des toilettes publiques ; la littérature démodée,
latin d’église, polar gay, romance animalière, porno de sacristie, théorie critique de nos grands-mères,
contes gores, poèmes de salon de coiffure, petits livres pour allumer les bûchers, opéras vieux, icônes érotiques, refrains niais,
techno littéraire, bottin de téléphone, guide touristique inachevé, mode d'emploi de magnétoscope, codes Microsoft, bilans sanguins.*

*Je rêvais : croisades, croisés massacrés, voyages dans les abysses,
fausse commune de mes communes défaites, histoires des tamarins empereurs, guerres de religion dans les salons de thé, révolutions chez les zèbres, immoralité
quantique des cantiques, tectonique des peuples, submersion des continents. Je croyais à tous les
désenchantements. J'étais KO, abîmé. J'avais deux trous rouges au côté droit. Je dormais.*


*J’inventai la couleur de mes suicides ! --- Revolver : noir. Revolver : blanc. Revolver : rouge. Revolver :
bleu. Revolver : vert. --- Cinq coups pour marquer les cycles de mon âme. Je réglai la forme et le mouvement de chaque détail. La mise en scène fonctionnait comme un train à vapeur.
Avec des gestes instinctifs, je me flattai d’inventer de nouvelles manières de mettre à mort. Soi ou les autres. Les autres d'abord ! La vapeur avait l'odeur des glaïeuls. J'attendais avec frisson l'augure :
un jour ou l’autre, au hasard d'une rue, dans tous les sens. J'offrirai le sacrifice à la beauté.
Les larmes auront le goût du cresson bleu. Et la nuque baignera dans le chanvre frais des bleuets.*

*Ce fut d’abord un petit tableau mignon. J'en fis la Sainte-Cène. Je peignais avec mes mots des silences, des nuits, je laissai
baver l’inexprimable. Je fixais des vertiges pour mieux me renier. Par mon seul parjure, je pouvais être. J'étais Arthur. J'étais Rimbaud. J'étais Arthur Rimbaud. Je sortais de ma tombe. Je retrouvais ma jambe. Je descendais des fleuves impassibles. J'allais loin, bien loin, comme un bohémien. Je me couchais dans un petit val. Je n'avais que 17 ans. Je m'entêtais affreusement. Je était un autre.*

*Loin des drones et des dromadaires, entre touristes et tourments, à genoux au pied de mon seul réverbère, comme à l'église aux pieds du prêtre, avant que je le châtrasse, je vomissais mon vin dans la tasse de quelque bourgeois. Le café, tiède et vert, avait une couleur de fiel et mon matin était plus beau qu'une messe. Qu'aurais-je bu dans un ciel sans nuit, éclairé de travail et de rancune, si ce n'est toutes les liqueurs de nos prières anciennes ? Cathodique jusqu'à l'os, mon stigmate s'évaporait dans les gilets jaunes que j'entassais sur ma peau crasse. Ô derme d'écorchures, ô couleurs de pisse. Tout faire pour faire de l'époque une cicatrice. Hurler parmi les haillons, sans confessions. Décapiter la sainte. Et suer pour qu'à l'odeur encore ils me connaissent. Les mastroquets s'allongeaient devant ma soif, de manière que l'orage de mes consommations mette la dernière pièce de mon dernier jour dans le creux de leur veine. Petite haine des boutiquiers nettoyant le silence qui suintait à mes lèvres. Souillé d'espoir, mon instant était au culte. J'y franchis le seuil. J'en porte encore la marque.*

*Ma vie est usure, et j'aime le bruit des égouts lorsque les anges s'y repaissent. Leur aurore demeure une vertu, avec cette économie des gestes qui fouillent la chaux comme les prophètes s'armaient contre les astres. À ma seule épiphanie, tout contre mon heure, je retourne la lame.*

*Je rêve d'or et d'Éthiopie, et j'irai à ma boisson jusqu'au désastre. Il est l'heure des aubes assassinées, je suis le désamour des sommeils, et à mon tour, je montrerai mes armes, paisible, je les alignerai sur des parterres de soie, j'y déposerai la myrrhe et le laurier, et j'y passerai ma paume lentement pour sentir chacune de nos entailles. Mes lèvres baiseront les canons à la très haute façon des mauvaises filles qui savent faire d'une ruelle un palais. La saveur de la poudre sera la beauté même, et par la transe antique de mes sœurs, je ferai de mes larmes une cathédrale --- éternité de la pierre et des soupirs, je t'abattrai. L'odeur de nos fins pour unique fête.*

*Sous les néons, aux sources de nos souterrains, il y a l'esclave qui dévide sa nuit pour que le jour s'efface toujours un peu plus. Les chemises sont tachées de bile, elles ont le goût du sucre et de l'abattoir. Les longueurs rousses de leurs bras nus me murmurent les transsubstantiations de Rahab. Elle porte ma nuit au-devant de la flamme. Je m'y gorge, comme avant le désert. Et de faux cieux se brisent à ma venue, pour que mes pieds dégouttent une sève éteinte.*

*L'ouvrier fuit mes oracles. Sa course trace des croix sur nos murailles, et pourtant il saignera sur ma jambe, y laissera les traces de ses peines. Le rêve sera laiteux sous ma paupière. Y tourneront ses marteaux, s'y retourneront ses chaînes. Une ville qui s'émiettera sur nos ombres --- déjà je souffle à son envie le vermeil et l'eau-de-vie. Les bombardements au loin. Douceur, minuit bas, le lin blanc sur l'argile, je frotterai le lapilli sur sa plaie jusqu'aux dernières lueurs des dernières frappes. L'ogive aura la forme de la chimère. Tendres griffes des nuages. Et je dormirai tout son jour pour que survive avec lui l'extinction de nos voix. La vieillerie de ma langue contaminera les agitations vénériennes du langage. Mais il sera trop tard. La langue demeurera une sclérose, le langage une fêlure.*

*Je m’habituais aux hologrammes publicitaires. Je voyais se matérialiser devant moi à chaque coin de rue mes désirs les plus profonds
sous la forme de faisceaux lumineux tridimensionnels. Un bolide à la place de ma vieille caisse, un palace à la place de mon 2 pièces,
un troisième œil robotique à la place du canapé clic-clac.*

*Puis je chiffrai mes sophismes magiques avec l’hallucination binaire du chiffre !*

*Je finis par trouver sacré le désordre de mon esprit. J’étais le pirate en proie
à une lourde fièvre : j’enviais la félicité des taupes, --- je mâchais longuement les chenilles, qui
représentent jusqu'à ce jour mon innocence recouvrée. Je me faisais de l'ombilic des limbes un boa pour que le marlou cesse de croire au sommeil de ma virginité.*


*Mon sexe s’aigrissait. Je le découpai en une infinité de manières d'être. Je disais adieu au monde dans d’espèces
de romances.*

*Abattre des tours revient à construire des tunnels.*

*Qu'il vienne, le temps des sacrifices. Nous sortirons de nos esprits embués l'idole, l'hydroxyde et le fer. Nous dresserons un autel plus large que le songe que nous fîmes cette nuit d'été où la révolte gronda jusqu'à nos lèvres. L'oubli refusera de nous revenir après les sacrilèges recommencés que nous porterons à la mémoire des cieux. Notre soif sera malsaine, et nous nous réjouirons d'y goûter l'huile et la pierre. Qu'il vienne, le temps des souillures. Nous nous éprendrons des terres trahies par leurs gestes pauvres. Nos ombres seront des légendes, et nous gratterons les enfers jusqu'à y déterrer l'ambre où brillent les espérances et le hurlement. L'ivraie se répandra sur la faim et nous creuserons en nos ventres des cavernes à la mémoire de nos frères bannis. Nous y graverons le verbe saint, et laisserons les insectes tournoyaient au-dessus de nos repos. Qu'il vienne, le temps des ruines --- qu'il nous prenne jusqu'à l'étouffement.*

*I like : le désert, les vergers brûlés, les boutiques fanées, les boissons
tiédies, les ruelles puantes, les pigeons écrasés par la circulation, les moucharabiehs, le pli des paupières, regarder le soleil jusqu'à l'aveuglement,
les agents de la maréchaussée qui trébuchent, les embûches, les supertankers, un vieux canon qui ne fonctionne pas (et puis soudain si, mais pas comme il faut),
les barres d'immeubles dans les pays de l'Est, les salons littéraires avec des squelettes partout, la glace à la pistache vert fluorescent, les barricades, les pissotières, les magasins splendides, les gens qui disent « splendide ! », les gens qui disent « ma petite dame » à de vieux messieurs, les gens qui volent les cannes de ces vieux messieurs, les bombardements au phosphore, le Trecento,
manger avec ses mains, les mains sales des ouvriers, les villes fantômes, les fantômes aussi, voir un film sur un écran fissuré, les gargouilles, les passants qui sifflotent, l'hydroxyde de fer,
les boudoirs,
les crucifix sans croix, les punks qui frappent des hippies, les chevalements, la cocaïne bio, les yeux rubis, les moucherons, les foulques macroules, les bourgeois neurasthéniques, les journaux qui noircissent les doigts, l'odeur du bitume les soirs d'été, la société des mouettes, l'émail des chasses d'eau, les fougères, le bruit des insectes, les tampons de la poste, la bourrache, le rayonnement infrarouge, les voitures retournées juste avant qu'elles prennent feu.*

*Makkke money with that! Bastards!*

*J'ai faim. Que l'on m'apporte les restes de mon mariage !*

*Je lèche longuement le calcaire que je foule avant de jeter mon œil à la terre. Dans le silence des sables, je pare mes entrailles de silice et de cuivre. Je perce mes solitudes des mutations dont le citoyen m'accuse. Je suis un monstre. Je n'ai pas de sexe. Je suis le fer qui se reproduit. Mes faims sont aussi multiples que mes victimes. Je leur accorde la mobilité de mes cruautés. Mon venin aussi aiguisé que mon vice. Mes crocs aussi doux que mon désir. Tout exsude. Et le lys blanc, arraché d'une main indolente, montre le fourmillement terreux de son germe. C'est entre les racines que va l'appétit de mes aubes. Le temple est une brisure. J'y dresse l'autel, et sur l'autel, calice vide, demeurent encore le rasoir et ma soif. L'hostie a le goût des galets que l'on jette au père. Les lieux sont vides, sans ombres, j'ai faim encore d'écroulement.*

*J'ai soif d'étreinte. Mangez-moi !*

*La gueule bourrée de plumes, je souris à ma forêt qui se consume au fil de mon échappée. J'ouvre suffisamment mon âme afin que viennent se recueillir toutes les araignées abandonnées du siècle. Qu'elles y trouvent le nid et l'amitié, qu'elles pondent leur promesse auprès de mes fruits. J'y cueillerai des langueurs dont je me parerai dans l'attente de mes holocaustes. Le loup abattu aura la même saveur que le lichen. Je gratterai l'intérieur d'un chêne, ensemble nous nous y allongerons, comme la rouille qui embrasse les portes de ma prison. Je peindrai l'azur de bistre pour qu'il se confonde à ma vengeance, et que l'amant ne distingue ma morosité du mouvement de ma dague. Sous une mer gelée, ensemble nous nous y allongerons. Seul le feu dégagera de ma mémoire l'espoir de quelques suffrages.*

*Mais l'arbre est rongé. Je goudronne ce que je peux de mes restes. Et que la vermine qui peuple ma coiffe se souvienne de l'ordalie, de ses étincelles plus ternes que ne le furent jamais nos fuites. Je remonte l'époque de mon tombeau, j'y quête le supplice. Les lendemains ont le goût de l'acide. J'y savoure la haine qui me précipite dans les braises --- je resterai à jamais leur seule marâtre. Les soleils attendent de brûler mon bois. Il est temps. L'éternité pour une image de mon brasier.*

*"Je devins un opéra fabuleux : je vis que tous les êtres ont une fatalité de
bonheur : l’action n’est pas la vie, mais une façon de gâcher quelque force, un
énervement. La morale est la faiblesse de la cervelle", disait Sappho.*

*Je fossoie davantage. De chaque chose, plusieurs autres vies me semblent dues. Ce petit monsieur ne sait
ce qu’il fait : il est un ange que j'enculerai. Nous connaîtrons la félicité des martyrs. Dans notre sein s'épanouira une nichée de chiens. Devant
plusieurs hommes, nous gesticulerons nus. Accrochés à des fils électriques, dans des mantes orange. Ma rage cause tout haut, lorsque, dans un instant d'éclat, je vois les trente-sept éléments de mon éveil qui tournoient au-dessus de mon épée. Le chemin tracé. Je parcours l'Érèbe à la recherche de la Vierge.
--- "Ainsi, j’ai aimé un porc", disait Hildegarde von Bingen.*

*"Les sophismes sont des vices de mouettes", disait Sei Shônagon.*

*"Les mouettes sont vicieuses par nature", disait Marguerite de Navarre.*

*"Aucun de mes vices ne vous concerne", répétait le porc, répétaient les mouettes.*

*Aucun sophisme pour les cloportes ! --- La folie ou que l'on m'enferme ! Mes erreurs cent fois répétées.
Je pourrais éviter de les redire toutes encore une fois, mais je tiens à détruire le système.*

*Leur santé. Leur terreur. Leur chute. Leur sommeil. Leurs jours. Leur lever. Leurs habitudes. Leur continuité. Leurs rêves.
Leur tristesse. Ils sont
mûrs pour le trépas, et par une route de dangers leur faiblesse les mènera aux
confins de l'Empire, Babylone en ruine, l'hypertexte qui déploie son tourbillon pour avaler la représentation de leur existence.*

*Je dus m'échapper pour échapper à leur plainte. Mon égarement dans les plaines sèches de l'enchantement me divertit un temps. Je voyais de grandes assemblées se tenir dans mon cerveau. Je fermais les portes. Et je jetais le feu.
Dans le désert, que j’aimais comme s'il eût dû me dépouiller de mes dernières offenses, je voyais
la croix se retourner dans mon sillage. Imprécations consolatrices pour la silice. Qu'ils soient damnés. Que l’arc-en-ciel les cisaille. Le Bonheur
sera leur fatalité, leurs remords, leur ver. Je soufflerai sur les braises qui agiteront le retour dse ruines. Mon souffle portera toujours plus loin
pour être dévoué à une quelconque civilisation.*

*Le Bonheur ! Au feu le Bonheur ! Sa dent perce l'œuf, la &#9760; y suinte, mais aucun citoyen des fluidités ne sait entrevoir derrière les superficialités de l'invisible. Le marché grouille de simulacres, tout erre à la suite de l'image. --- Il faut renier la main tranchée qui dicte le chemin. Mais le chant du coq m'avertit qu'il est l'heure des exécutions, --- ad
matutinum, au Christus venit, --- dans la plus sombre des villes nous creusons le fossé :*

*Nous abattons encore des châteaux pour nous divertir des mortes-saisons. Notre âme s'y glisse, y caresse longtemps le grès. Le bouleau y est plus noir que le sort que nous jetions autrefois aux gens armés des villes. Les forts fument et les avenues sont à présent de rases campagnes. Nous avons suivi le rite, nous avons coupé les pattes du coq, nous avons placé son cœur sur notre langue et avons revu le lynx et le chacal. Nous avons lentement passé la lame sur notre front et nous avons délivré Lilith de son secret. Nul effort, nul pardon. Seule notre brûlure pour que brûle encore leur monde. Il y a de l'acier jusque dans leur fuite, hélas, ils nous retrouveront, et l'acier à nouveau servira à dresser des châteaux et des saisons. Mais contre leurs châteaux et leurs saisons, face au satyre et à l'éternel, nous jurons de demeurer l'implacable --- nous sommes la vengeance.*

*C'est ainsi. Je sais aujourd’hui cracher au visage de la beauté.*

*Les pavés ont la forme des crânes qu'ils épousent.*

*Cette vie de mon enfance : la grande autoroute, la pluie, ivre
surnaturellement, plus intéressé que le meilleur des mendiants, fier de
n’avoir plus d'argent, plus de pays, plus d'amis, quelle sottise la vie. --- Et je m’en aperçois
seulement !*

*Les crânes deviennent pavés à force d'être piétinés.*

*--- J’ai eu raison de dédaigner ces bonshommes qui ne perdaient pas une occasion
de caresser le parasite que je suis ; je préfère me situer du côté de la propreté et de la santé des femmes,
aujourd’hui qu’elles sont encore si peu nombreuses à me considérer comme leur sœur.*

*J’ai même eu raison de mon orgueil : puisque je m’évade !*

*Je m’évade ! Je t'évade !*

*Je m’explique. Je t'explique.*

*Hier encore je soupirais : « Ciel sommes-nous assez de damnés ici-bas avons-nous
passé tant de temps déjà dans leur troupe nous les connaissons tous nous nous
reconnaissons toujours nous nous dégoûtons la charité nous est inconnue
mais nous sommes polis nos relations avec le monde sont très-très-convenables
est-ce étonnant le monde les marchands les naïfs nous ne sommes
pas déshonorés mais les élus comment nous recevraient-ils hors de leurs palais il y a des
gens hargneux et joyeux de faux élus puisqu’il nous faut de l’audace ou de
l’humilité pour les aborder ce sont les seuls élus ce ne sont pas des
bénisseurs ce sont des faux-monnayeurs nos semblables !»*

*Retrouver deux sous de raison --- ça passe vite --- je vois mes
malaises --- venir à bout --- figurer l'Occident --- assez tôt pour fuir
--- ou pour le corrompre --- les marais occidentaux --- non que je croie la lumière altérée ---
la forme exténuée --- le mouvement égaré --- corruption des élus --- bon augure ---
voici mon esprit --- vouloir absolument --- se charger de tout --- les développements cruels
--- subir l’esprit --- depuis la fin de l’Orient --- en vouloir à mon esprit !*

*Je suis une balle traçante dans la nuit d'Irak, je suis la lunette de vision nocturne qui éteint le ciel d'Orient, je suis l'œil du drone, je suis le phosphore qui perle l'enfance, je suis la mine antipersonnel après son clic, je suis la couleur verte, de la foi ou de l'écran du radar, je suis un tapis de prière rongé par l'acide, je suis le fusil qui demeure plus droit que la morale, je suis une bassine dans laquelle on simule la noyade, je suis le muscle de l'interrogatoire, je suis l'ogive qui a la forme du minaret, je suis une bouteille de plastique pleine de sable, je suis l'inventivité du supplice, je suis l'enthousiasme d'une guérilla, je suis sa contrebande, je suis la banque qui blanchira son enthousiasme, je suis le puits, je suis le pétrole qui brûle plus noir que la nécrose, je suis le viol de la femme, je suis son voile, je suis l'intégral, viol et voile, je suis la tuméfaction, je suis la larme, seule, je suis la lame télévisuelle qui tranche la gorge du touriste, je suis l'émoi d'Occident, et je suis son oubli, je suis une haine religieuse, je suis un intérêt géostratégique, je suis le satellite qui se confond aux étoiles, je suis la pierre qui roule sur le hurlement, je suis la fracture et la fracture de l'os, je suis la beauté de la moelle, fracture ouverte, je suis l'agonie, je suis les gravats plus importants que le souffle qu'ils masquent, je suis le sang qui coule sur l'argile d'une ruine millénaire, je suis la ruine millénaire, je suis la finance qui veut reconstruire la ruine à l'identique, je suis le cours du baril, je suis le ventre troué d'un passant, je suis le hasard, je suis le ver qui naît de la fosse commune, je suis l'indifférence du ver, je suis la langue arabe que l'on résume au mot de guerre sainte, je suis sa poésie exécutée à l'aube, je suis une famille qui gouverne, je suis la peur de la famille qui gouverne, je suis un câble électrique que l'on attache à un sexe, je suis un cri qui n'existe pas, je suis l'odeur de la pisse, je suis une geôle clandestine, je suis du barbelé, je suis le fil de fer dans lequel on enroule la pensée politique, l'opposante, je suis la solidarité internationale, je suis la publicité qui interrompt le bulletin d'information, je suis un camp de réfugiés, je suis une loi internationale qui interdit de bombarder les camps, je suis le bombardement quand même, je suis l'erreur, je suis le rêve, je suis l'erreur ou le rêve de la négation d'Orient, je suis la migration, je suis l'embarcation, frêle, je suis la vocifération d'un expert qui attend l'embarcation, je suis un passeur qui conserve les papiers des sans-papiers, je suis l'étrange, je suis l'étranger, je suis l'extrême droite qui ne sait pas vivre sans l'étranger, je suis une pièce que l'on donne pour les malheureux de la guerre, je suis un don déductible des impôts, je suis la geste des pouilleux, je suis le reste de la bourgeoisie, je suis sa bonne pensée, je suis son racisme sous sa bonne pensée, je suis son silence qui se satisfait du silence des naufrages, je suis le chagrin, je suis le spectacle, je suis le dîner en ville, je suis un rire, je suis une plainte à un dîner en ville, je suis la phrase "c'est affreux de laisser mourir des gens comme ça", je suis "comme ça", je suis l'addition à la fin du dîner, je suis le prix de l'essence nécessaire pour rentrer chez soi, je ne suis pas un corps, je suis le corps de la noyée, gonflée, je suis son passeport perdu dans l'embrun, je suis un peu d'orange parmi l'azur, je suis le gilet de sauvetage sans sa dépouille, rangé dans une cabine, je suis le luxe de la cabine, je suis l'indolence d'un yacht qui croise au loin, je suis les cales du yacht dans lesquelles dort le personnel, je suis un téléphone portable qui prend une photographie sur le pont, je suis l'autoportrait devant la Méditerranée, je suis le réseau social qui trafique l'ego, je suis la mouette qui observe l'exil avant de déchiqueter sa chair qui flotte, je suis une charogne qui ressemble à du bois de grève, je suis un journal télévisé, je suis l'indignation d'un journaliste, je suis le sommeil, je suis la télécommande qui tait le décompte des naufragés, je suis le malheur, ici, je suis son secret, je suis la civilisation que l'on plastique, je suis la Babylone d'Europe, je suis une survivante, furtive, je suis ma cache, je suis mon commerce, je suis la police qui me surveille, je suis le soir, je suis la passe, je suis un appel de phare, je suis la capote trouée, je suis une pute à la mémoire plus longue que le siècle, je suis l'attente et l'effondrement, je suis le vent, je suis l'herbe qui recouvrira la ruine, je suis l'inespoir d'Orient.*

*...Mes deux sous de raison sont finis ! --- L’esprit d'autorité me capture sur une aire d'autoroute. Il veut que
je sois l'Occident. Il ne me reste que ma petite monnaie d'insane : « Pour conclure comme je voulais taire le faire il faudrait le faire pour conclure comme je voulais comme il faudrait si seulement je voulais taire me taire et faire je fais je tais il faudrait conclure ma volonté comme je tais ou je fais je conclus ma volonté de taire ou de faire je fais la conclusion je tais je fais la volonté le faire et le taire la volonté de conclure la volonté il faudrait me taire et faire je fais la volonté qui veut ma volonté libérée ».*

*Je plaçais aux nues les palmes des martyrs, les rayons de l’art, l’orgueil des
inventeurs, l’ardeur des pillards, l’Orient et ses sagesses,
la première et l'éternelle conjuration. --- Il paraît que c’est un rêve de paresse que de préférer la critique de la critique critique.
Benedictum Otium in Angelis suis, et in Sanctis suis.*

*Pourtant\
Je ne songeais\
Guère au plaisir\
D’échapper aux souffrances\
Modernes\
Je n’avais pas en vue\
La sagesse bâtarde du Coran\
Mais n’y a-t-il pas un supplice\
Réel\
En ce\
Que depuis cette déclaration\
De la science\
Le christianisme\
L’homme se joue\
Se prouve\
Les évidences\
Se gonfle du plaisir\
De répéter ces preuves\
Et ne vit\
Que comme cela\
Torture subtile\
Niaise\
Source de mes divagations\
Spirituelles\
La nature pourrait\
S’ennuyer\
Peut-être\
Les prud'hommes\
Naissent\
Avec le Christ*\

*Nous cultivons la brume. Nous mangeons son fruit. La fièvre de
nos légumes aqueux nous offre la vision. Et l’ivrognerie et le tabac. Et l’ignorance pour nous dévouer
à Dieu. Au loin la pensée. La sagesse de
l’Orient sous la lumière des drones. Le rhizome primitif. Pourquoi un monde moderne. Si de pareils
poisons s’inventent.*


*Les gens d’Église diront, un à un : « Je comprends. Mais vous voulez encore un peu de l’Éden. Vous désirez la pureté moins l'autre.
Rien que pour vous. Je commerce l’histoire des peuples. Les soumissions d'Orient sont de mes tributs. Je vous fais grâce des races. Elles sont plus vraies que leurs songes serviles d'égalité. Je comprends. C'est pourquoi je châtie la créature des abysses. Je n'accorde aucune indulgence à l'esclave des esclaves de ses frères. Ma bonté pour l'unique Éden !
L'espace de toutes nos bontés. Le royaume à la blancheur immaculée. Sans le regard qui se porte au-delà des fleuves de Koush. Qu’est-ce que mon rêve, si ce n'est la souffrance éternelle du fils de Cham ? Cette pureté des
races antiques. Par le sang sombre du charbon. Il ne nous reste que notre pureté virginale à défendre des ombres. » Tout ce qui restera au-delà des croyances, tout ce qui subsistera
même après les gens d'Église. Je l'abattrai et le couvrirai de sel. Paresseux crapauds de bénitier. Je vous maudis trente-sept fois.*

*Philosophesses Confesse : Âge Sexe Ville. Genre Monde Destinée. Humanité Inhumanité, Inhumanité Simplement.
Oxydation Occident. Occire Occident. Libre libre. Sans habits, Sans habitat. Sabbat. Orient Horreur. Ancien Sien Faille Défaille. Bien ou Bien.
Soie Soyez Soyeuses. Ne Soyez. Vain Vaine Vaincue. Cul Par Terre.
Philosophesses Plein Les Fesses : votre Occident ou Vôtre. Se Vautrer Contre Culture.*

*Tempête sous crâne : mon esprit, prends garde pas de partis de salut violents exerce-toi
ah la science ne va pas assez vite pour nous
mais je m’aperçois que mon esprit dort
s’il était bien éveillé toujours à partir de ce moment nous serions bientôt
à la vérité qui peut-être nous entoure avec ses anges pleurant s’il
avait été éveillé jusqu’à ce moment-ci c’est que je n’aurais pas cédé aux
instincts délétères à une époque immémoriale s’il avait toujours été
bien éveillé je voguerais en pleine sagesse. Crâne crame sagesse.*

*Ô l'impur ! Impur !*

*Délier mon aliénation : « Qui de pureté la vision m’a donné d’éveil cette minute c’est cette minute avant la pureté c'est l'éveil qui donne l'impur l'impureté ou la pureté en allée de ma vision l'éveil en cette minute d'impureté qui m'a donné l'éveil et la vision les cinq facultés et les cinq pouvoirs les sept facteurs de l'impur j'ai la vision et je me donne entièrement à cette minute de l'éveil. --- Par
l’esprit, on va à Dieu ! Et on le renverse. »*

*Déchirante fortune !*

*Je lèche des éclairs en attendant l'aurore.*

*Le travail personnifie l'inhumain ! Il est l’explosion qui éclaire mon abîme. La servitude du temps au temps.*

*Je lèche l'aurore en attendant les éclairs.*

*Rien n’est vanité\
À la science\
Et en avant\
Cri d’Ecclésiaste\
Moderne\
C’est-à-dire\
Tout le monde\
Et pourtant\
Les cadavres des méchants\
Et des fainéants\
Tombent\
Cœur des autres\
Ah ! vite vite\
Un peu\
Là-bas\
Par-delà la nuit\
Récompenses futures\
Éternelles\
Échappons-nous ?\
Qu’y puis-je ?\
Je connais le travail\
Et la science est trop lente\
Prière au galop\
Que la lumière\
Gronde\
Je le vois bien\
C’est trop simple,\
Et il fait trop chaud\
On se passera de moi\
J’ai mon devoir\
J’en serai fier\
À la façon de plusieurs\
En le mettant de côté*\

*Mon code a glitché. Allons, feignons, fainéantons, ô pitié, et nous existerons,
nous amusant, en rêvant, amours, monstres, univers fantastiques, en nous,
plaignant, querellant, les apparences du monde, saltimbanque, mendiant,
artiste, bandit, prêtre ! Dans l’interstice hypertextuel, les sens me manquent, gardien, aromates sacrés, confesseur, martyr --- moi !*

*Je reconnais là ma sale éducation d’enfance. Puis quoi quoi quoi aller mes vingt
ans, mes dix doigts, la gueule de l'inconnu. Ô aime-moi !*

*Non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! non ! à présent je me révolte.*

*Día de &#9760; ! Le travail bien fait.*

*Paraît qu'il est trop
léger à mon orgueil : ma trahison au monde en sera le supplice. Haut et court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! court ! au
dernier moment, à droite, à gauche, personne, je couperai furtivement la corde.*

*Que la vie soit donnée !*

*Alors, --- oh ! --- chère pauvre âme --- oh ! --- l’éternité --- oh ! serait-elle --- oh ! --- pas perdue --- oh ! --- pour
nous --- oh réveille-toi !*

*La cendre mâtine ma diane.*

*N’eus-je pas une fois une jeunesse aimable, héroïque, fabuleuse, à écrire sur
quelque thread, --- trop de malheur ! Par quel crime, par quelle erreur,
ai-je mérité ma société actuelle ? Vous qui prétendez ne pas être de ma tribu, je prétends appartenir au peuple des bêtes qui poussent
des sanglots longs, expriment l'éternité du chagrin, désespèrent des hommes, pansent les morts, rêvent les malades, se parent du mal, mes bêtes,
(debout !) et tâchez de raconter notre chute et notre sommeil. Moi, je ne puis plus
m’expliquer que le mendiant de likes avec ses continuels Pater et Ave Maria ne voient en lui la bête qui chute ou qui sommeille. Je ne
sais plus parler ! Sais-je encore écrire ? Pattern austère... Vous qui êtes à l'ICANN !*

*Changer de chaînes :
pourtant·aujourd·hui·je·crois·avoir·fini·la·relation·de·mon·enfer·c·était·
bien·l·enfer·l·ancien·celui·dont·le·fils·de·l·homme·ouvrit·les·portes·
du·même·désert·à·la·même·nuit·toujours·mes·yeux·las·se·réveillent·à·l·étoile·
d·argent·toujours·sans·que·s·émeuvent·les·rois·de·la·vie·les·trois·mages·le·
cœur·l·âme·l·esprit·quand·irons·nous·par·delà·les·grèves·et·les·monts·
saluer·la·naissance·du·travail·nouveau·la·sagesse·nouvelle·la·fuite·des·
tyrans·et·des·démons·la·fin·de·la·superstition·adorer·les·premiers·
noël·sur·la·terre·
le·chant·des·cieux·la·marche·des·peuples·esclaves·ne·maudissons·pas·la·vie.*

*Le chant des peuples, la marche des cieux ! Esclaves, ne vivons pas maudit.
La marche des esclaves, le chant des peuples ! La vie, ne maudissons pas les cieux.
LA VIE MAUDITE DES PEUPLES ESCLAVES LA MARCHE DES CIEUX NE PAS !*

*Nos bonnes consciences saliront nos renaissances à la pureté.*

*Automne déjà !\
Mais ! Pourquoi !\
Regretter !\
Éternel !\
Soleil !\
Sommes !\
Engagés !\
Découverte !\
Clarté !\
Divine !\
Loin ! Les gens !\
Ils meurent !\
Sur nos saisons !*\

*La pureté viendra à bout de nos bonnes consciences.*

*L’automne déjà ! Notre barque élevée dans les brumes immobiles tourne vers le port de
la misère la cité énorme au ciel taché de feu et de boue ah les haillons
pourris le pain trempé de pluie l’ivresse les mille amours qui m’ont
crucifié elle ne finira donc point cette goule reine de millions d’âmes et de
corps morts et qui seront jugés je me revois la peau rongée par la boue et
la peste des vers plein les cheveux et les aisselles et encore de plus gros
vers dans le cœur étendu parmi les inconnus sans âge sans sentiment
j’aurais pu y mourir l’affreuse évocation j’exècre la misère.*

*Et parce que du confort la saison c’est l’hiver je redoute la saison l'hiver le confort parce que du confort l'hiver ou la saison je redoute le confort de la saison c'est l'hiver c'est le confort qui doute je suis l'hiver qui redoute la saison du confort je suis le doute le redoute je redoute le confort le réconfort la saison ou l'hiver je suis l'hiver qui doute !*

*[quel-que-f01s-je-v01s-au-c1el][des-pla-ges-sans-f1n][c0u-ver-tes-de-blan-ches-na-t10ns][en-j01e][un-grand-va1s-seau-d’0r][au-des-sus-de-m01][a-g1-te-ses-pa-v1l-l0ns][mul-t1-c0-l0-res][s0us-les-br1-ses-du-ma-t1n][j’a1-cré-é-t0u-tes-les-fê-tes][t0us-les-tr1-0m-phes][t0us-les-dra-mes][j’a1-es-sa-yé-d’1n-ven-ter][de-n0u-vel-les-fleurs][de-n0u-veaux-as-tres][de-n0u-vel-les-cha1rs][de-n0u-vel-les-lan-gues][j’a1-cru-ac-qué-r1r][des-p0u-v01rs-sur-na-tu-rels][eh-b1-en][je-d01s-en-ter-rer][m0n-1-ma-g1-na-t10n][et-mes-s0u-ve-n1rs][une-bel-le-gl01-re-d’ar-t1ste][et-de-c0n-teur-em-p0r-té-e][m01-m01][qu1-me-su1s-d1t][0-0u-1][d1s-pen-sé-de-t0u-te-m0-ra-le][je-su1s-ren-du][au-s0l][a-vec-un-de-v01r][à-cher-cher][et-la-ré-al1-té-ru-gueuse][à-é-tre1n-dre][pa-y-san][¿su1s-je-tr0m-pé?][la-cha-r1-té-se-ra1t-el-le][sœur-de-la-&#9760;][¿p0ur-m01?][en-f1n][je-de-man-de-ra1-par-d0n][p0ur-m’ê-tre-n0ur-r1][de-men-s0nge][et-al-l0ns][ma1s-pas-un-e-ma1n-a-m1e][¿et-0ù-pu1-ser-le-se-c0urs?]*

*Idiot. Un soir, je dus voyager. Ainsi, j'en prenais l'habitude. Vivre somnambule. Je croyais à tous les enchantements. Cette bouche sous tes yeux. Nous nous accordions. N'est-ce pas parce que nous cultivions la brume ? L'automne déjà. Je crois avoir fini. C'était bien l'enfer. Esclaves, ne maudissons pas la vie. Il faut être absolument moderne. Ou idiot. Horreur de ma bêtise. L'heure nouvelle. Une main amie. Une main de plume. Une autre de charrue. Idiot. Un soir, je dus voyager. J'avais entrevu. Entrevu : faim, soif, cris, danse, par des nuits d'hiver. Quitter ce continent, quitter la vision, elle. Vivre somnambule. Feu sur moi. Je meurs de soif. Je m'y habituerai. Je suis faible. L'idiot qui a perdu la sagesse. Veut-on des chants ? Des danses ? Elle. Le désert. Tout de suite, je suis tombé. Jamais jaloux. Nous nous accordions. J'aimais, je rêvais, j'inventai. J'écrivais des silences, des nuits, fixais des vertiges. L'odeur du soir fêté. Idiot. Un soir, je dus voyager. Distraire les enchantements assemblés sur mon cerveau. L'eau des bois se perdait sur les sables vierges. J'aimai le désert. Elle, était ma fatalité, ma vie serait toujours trop immense pour être dévouée à la force et la beauté. J'étais mûr. Idiot.*

*L’heure nouvelle : tirer sur les horloges !*

*Car je puis dire que la victoire m’est acquise : les grinceurs de dents, les
siffleurs de feu, les empestés, les plaintifs, les modérés, les nostalgiques, les
immondes, les citoyens s'effaçant devant l'autorité, les passagers de l'aube, les dernières silhouettes qui détalent, les jaloux, les
mendiants du pouvoir, les brigands ès lettres, les amis de la &#9760;, les arrière-gardes, les sortilèges, les bovins et les prêtres.
--- Damnés, si je me vengeais !*

*Il nous faut être absolu, absous, abstrus, moderne, antimoderniste, chiens pour nos contemporains. Gueules ouvertes, évidence des crocs.*

*Point-cantique : tenir le haut du pavé. Dure nuit. Le sang séché. Fume. Sur nos
faces. Il semble. Que nous n'avons plus rien. Derrière nous. Cet horrible arbrisseau. Le combat
spirituel. Brutalité policière. Porter l'œil unique. Fier. Contre les ors. La bataille des Untermenschen. Splendeur des bas-fonds. Vision. Ajuster. Justice. LBD 40. La justice n'est pas aveugle. Elle est borgne. Point-quantique-des-cantiques : Dieu seul se masturbe devant son peuple. La nuit noire. Les lendemains. Courbatures.
Néanmoins. Cependant. Toutefois. C’est la veille du jour d'après. Manifestation. Représentant syndical qui nous nuit. Nuisances. Retour de nuisances. Tête de cortège. Esseulement. Recevoir tous les influx. De vigueur et de tendresse.
La réalité n'est rien. Le crépuscule. Lacrymogènes. L’aurore des lasers. Armes contre paroles. Dissymétrie. Une patiente ardence. Révoltes, révoltés, révoltants. Complaisance du geste manifestant. S'offusquer des casseurs. Néant. S'observer manifester. Néant. Aucune révolution possible. La commune des petits messages. Sur de
splendides villes. Néant. Occupation des théâtres. Les ministères sont à côté. Néant. Revenir à son téléphone. S'indigner un peu. Néant. Dîner petit-bourgeois. Néant. Manifestation. Néant.*

*Être le nihiliste du néant : « Sans citoyenneté, bras à bras, chargés de nous-mêmes, du poids de nos remparts, des transferts de nos blocages, nous remplacerons de notre souffle corrosif l'air vicié d'or et de marbre. »*

*À qui adressions-nous ces prières du néant ? À qui parlions-nous de ces châteaux évanouis dans le petit matin de nos colères ? La main amie demeure celle qui frappe la pierre à la recherche d'une fissure. Que l'on vienne répondre à nos rires par de
vieilles amours mensongères, que l'on porte la prison au cœur de nos songes. Nous nous vengerons des architectures, nous secouerons les tours menaçantes auxquelles se suspend le tourment. --- Souvenirs de l'enfer. Nous vîmes
le caveau des femmes et les instruments de leur obédience. --- Revenir de nulle part. --- Là-bas ; --- et il nous sera loisible de nous déposséder des
vérités affirmées qui restreignent l'âme au corps, et le corps au corps de l'homme.*

*Allium tuberosum,*\
\
*Je mâche l'expression de tes gènes.*\
*Dans la feuillée, écrin vert taché d'orgueil,*\
*Dans la feuillée incertaine et fleurie*\
*De fleurs splendides où le baiser dort,*\
*Vif et crevant l'exquise broderie*
\
*Sous ma machénanique maxillaire,*\
*Tes arômes, tes saveurs, ta texture,*\
*Hystériques, la houle à l'assaut des récifs,*\
*Qu'exsude tant de plaisir que j'enrage.*\
*Des communs élans*\
*Là tu te dégages*\
*Et voles selon.*\
\
*Jiǔcài,\
Tu nous as porté vivant,\
De tout notre poids d'amour.\
Jouant à la guerre, nous te foulions herbe comme les autres,\
Herbes folles ou mauvaises amaryllidacées ;\
Échouages hideux au fond des golfes bruns\
Étaient les seuls horizons de notre humanité.*\
\
*韭菜,\
Que d'ADN extrait depuis ta tétraploïdie sur notre paillasse de cuisine aux croyances parenthétiquement défaites du phénomène des apparitions et des héritages.\
Mélange de coups de pilon, de lipophile, d'alcool, que nous t'infligions\
Aux raisons de L’âcre amour qui nous gonflait de torpeurs enivrantes.\
Te mettre en tube et t'observer,\
öes saveurs de ta double hélice dans sa chambre,\
À peine dévoilée, suspendue,\
L’alcool dénaturé était le voile de nos nudités communes.*\
\
*Pourtant tu es `4 n` et nous sommes de `2`.*\
\
*Pinyin\
Là pas d'espérance,\
Nul orietur.\
Science avec patience,\
Dans la douceur de ton mariage avec ma salade bretonne,\
Je t'implore de mordre aux fraîcheurs de ma Rémission.\
Repentir ne me sera pas épargné,\
Je suis noosphériquement responsable et coupable.*\
\
*J'ai découvert un peu de ta vie en me noyant dans ton monde, je me suis enivré des tes histoires, j'ai assumé le passé de mes aĩeules et mes aïeux qui t'avaient invisibilisée, colonisée, exploitée, acclimatée, désherbée. Je tente débelliqueux. J'ai déconstruit des passés et des présents replantés.*\
\
*Laisse-moi te ramener à Cancale.*\
*Un Tangon face à la baie,*\
*Ô flots*\
*ACABrantesques*\
*Prenez mon cœur*\
*Qu'il soit sauvé.*\
\
*Ci-GIT je*

*Germinal-thermidor, l'an 3073.*
