.PHONY: cleantex cleanall tout

#
# Variables
#

## Extension des fichiers textes (ex.: txt, md, markdown)
MEXT = md

## Tous les fichiers textes dans le dossier contenant le texte brut de l'ouvrage
SRC = $(sort $(wildcard texte/*.$(MEXT))) 
SRCBRUT = $(sort $(wildcard texte-src/*.$(MEXT))) 

## Le fichier contenant la bibliographie
BIB = bibliographie/sources/bibliographie.bib

## CSL stylesheet (un style possible pour la bibliographie)
CSL = bibliographie/styles/abrupt.csl

# Nom du fichier PDF produit (il porte le nom du dossier)
# NOM = $(shell basename $(CURDIR))
NOM = enfer

# Template
GABARIT = gabarit/livre.tex
GABARIT_COUVERTURE = gabarit/couverture.tex

#
# Spécificités du livre
#
hauteur = 175mm
largeur = 108mm
fondperdu = 5mm
dos = 8.1mm
titre_table = Table des matières
titre_bibliographie = Bibliographie
# Police (ebgaramond, librecaslon, librebaskerville, un autre paquet TeX de police,
# sinon modifier le gabarit avec le paquet fontspec pour une police particulière)
# En fonction du choix de la police, il faudra optimiser certains espacements
# Comme le tocnumwidth pour les chapitres (à agrandir par ex. pour le Baskerville)
police = crimson
# des options pour la police
# (par ex. lining pour un alignement des chiffres avec le Garamond, mais rien avec le Baskerville)
# policeoptions = lining
# Le corps est la taille de la police
corps = 12pt


html:
	pandoc -s -f markdown -t html --lua-filter gabarit/pandoc-quotes.lua --self-contained --top-level-division=chapter --citeproc --lua-filter gabarit/parentheses.lua --lua-filter gabarit/point-final.lua --lua-filter gabarit/typographie.lua --csl=$(CSL) --bibliography=$(BIB) --metadata reference-section-title="$(titre_bibliographie)" --toc --toc-depth=3 --template=gabarit/html/web.html --css=gabarit/html/web.css -o $(NOM).html $(SRC) && sed -i.bak 's/↩︎/↑/g' $(NOM).html && rm $(NOM).html.bak

tex:
	pandoc -s -f markdown -t latex --lua-filter gabarit/pandoc-quotes.lua --pdf-engine=lualatex --top-level-division=chapter --citeproc --lua-filter gabarit/parentheses.lua --lua-filter gabarit/point-final.lua --lua-filter gabarit/typographie.lua --csl=$(CSL) --bibliography=$(BIB) --metadata reference-section-title="$(titre_bibliographie)" --toc --toc-depth=3 -V toc-title="$(titre_table)" -V police="$(police)" -V policeoptions="$(policeoptions)" -V hauteur="$(hauteur)" -V largeur="$(largeur)" --template=$(GABARIT) -o $(NOM).tex $(SRC)

odt:
	pandoc -s -f markdown -t odt --lua-filter gabarit/pandoc-quotes.lua --top-level-division=chapter --citeproc --lua-filter gabarit/parentheses.lua --lua-filter gabarit/point-final.lua --lua-filter gabarit/typographie.lua --csl=$(CSL) --bibliography=$(BIB) --metadata reference-section-title="$(titre_bibliographie)" --toc --toc-depth=3 -o $(NOM).odt $(SRC)

pdf:
	pandoc -s -f markdown -t latex --lua-filter gabarit/pandoc-quotes.lua --pdf-engine=lualatex --top-level-division=chapter --citeproc --lua-filter gabarit/parentheses.lua --lua-filter gabarit/point-final.lua --lua-filter gabarit/typographie.lua --csl=$(CSL) --bibliography=$(BIB) --metadata reference-section-title="$(titre_bibliographie)" --toc --toc-depth=3 -V toc-title="$(titre_table)" -V police="$(police)" -V policeoptions="$(policeoptions)" -V corps="$(corps)" -V hauteur="$(hauteur)" -V largeur="$(largeur)" --template=$(GABARIT) -o $(NOM).pdf $(SRC)

couv:
	pandoc -s -f markdown -t latex --pdf-engine=lualatex -V police="$(police)" -V policeoptions="$(policeoptions)" -V hauteur="$(hauteur)" -V largeur="$(largeur)" -V fondperdu="$(fondperdu)" -V dos="$(dos)" --template=$(GABARIT_COUVERTURE) -o couverture.pdf gabarit/meta_couverture.md

epub:
	pandoc -s -f markdown -t epub2 --lua-filter gabarit/pandoc-quotes.lua --epub-cover-image=gabarit/ebook/couverture_ebook.jpg --css=gabarit/ebook/ebook.css --epub-embed-font=gabarit/ebook/fonts/Crimson-*.otf --template=gabarit/ebook/gabarit.epub2 --citeproc --lua-filter gabarit/parentheses.lua --lua-filter gabarit/point-final.lua --lua-filter gabarit/typographie.lua --csl=$(CSL) --bibliography=$(BIB) --metadata reference-section-title="$(titre_bibliographie)" --epub-chapter-level=1 --shift-heading-level-by=0 -o $(NOM).epub $(SRCBRUT) && ./gabarit/ebook/modification.sh

brut:
	pandoc -s -t markdown --top-level-division=chapter --markdown-headings=atx --wrap=auto --citeproc --lua-filter gabarit/parentheses.lua --lua-filter gabarit/point-final.lua --citeproc --csl=$(CSL) --bibliography=$(BIB) -o $(NOM).md $(SRC)

livre:
	./livre.sh

cleantex:
	latexmk -c && rm -f *.xml

cleanall:
	latexmk -c && rm -f *.xml && rm -f *.html *.odt *.md *.epub *.idx *.tex *.pdf

tout: livre couv epub brut odt html
