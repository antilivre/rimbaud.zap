// Scripts

// Menu
const reloadBtn = document.querySelector('.btn--reload');
const menuBtn = document.querySelector('.btn--menu');
const menu = document.querySelector('.menu');
let menuOn = false;
const commitBtn = document.querySelector('.btn--commit');
let commitOn = false;

reloadBtn.addEventListener('click', (e) => {
  e.preventDefault();
  window.location.reload();
  menuBtn.blur();
});

menuBtn.addEventListener('click', (e) => {
  e.preventDefault();
  menu.classList.toggle('show');
  menuBtn.blur();
  if (commitOn) {
    const commit = document.querySelector('.version--show .commit');
    commit.classList.remove('show');
    commitOn = !commitOn;
  }
  menuOn = !menuOn;
});

commitBtn.addEventListener('click', (e) => {
  e.preventDefault();
  const commit = document.querySelector('.version--show .commit');
  commit.classList.toggle('show');
  if (menuOn) {
    menu.classList.remove('show');
    menuOn = !menuOn;
  }
  commitOn = !commitOn;
  commitBtn.blur();
});

const interfaces = document.querySelectorAll('.interface');
interfaces.forEach(el => {
  el.addEventListener('touchend', function(e) {
    document.body.style.overflow = "auto";
  });
});

window.addEventListener("touchstart", function(event) {
  if (event.target.classList.contains('interface') ||
      event.target.classList.contains('level') ||
      event.target.classList.contains('range')) {
    event.preventDefault();
    document.body.style.overflow = "hidden";
  }
}, false);

// Markdown
let markdownTexts = document.querySelectorAll('.markdown');
markdownTexts.forEach(el => {
  const reader = new commonmark.Parser({smart: true});
  const writer = new commonmark.HtmlRenderer({ safe: false, sourcepos: true });
  const parsed = reader.parse(el.innerHTML);
  el.innerHTML = writer.render(parsed);
});

// Dark mode
const btnDark = document.querySelector('.btn--dark');

let darkModeToggle = false;
const darkModeTheme = localStorage.getItem('theme');

if (darkModeTheme) {
  document.documentElement.setAttribute('data-theme', darkModeTheme);

  if (darkModeTheme === 'dark') {
    darkModeToggle = true;
  }
}

function darkModeSwitch() {
  if (!darkModeToggle) {
    document.documentElement.setAttribute('data-theme', 'dark');
    localStorage.setItem('theme', 'dark');
    darkModeToggle = true;
  } else {
    document.documentElement.setAttribute('data-theme', 'light');
    localStorage.setItem('theme', 'light');
    darkModeToggle = false;
  }
  btnDark.blur();
}

btnDark.addEventListener('click', darkModeSwitch);

// Baffle
let baffleTxt = baffle('.baffle')
  .start()
  .set({
    characters: 'RIMBAUD.ZAP',
    speed: 150,
  })
  .reveal(1000, 1000);

// Change color
const text = document.querySelector('.enfer');
const rangeFg = document.querySelector('.range--fg');
const rangeEcriture = document.querySelector('.range--ecriture');

rangeFg.addEventListener("input", () => {
  document.documentElement.style.setProperty('--color-fg', 'rgba(var(--color-alpha-fg),'+ rangeFg.value / 100 + ')');
});

rangeEcriture.addEventListener("input", () => {
  document.documentElement.style.setProperty('--color-ecriture', 'rgba(var(--color-alpha-ecriture),'+ rangeEcriture.value / 100 + ')');
});

/* History of the text */
const textVersions = document.querySelectorAll('.version');
const textVersionsNb = textVersions.length - 1;
const rangeHistory = document.querySelector('.range--history');
const emptyBoxLeft = document.querySelector('.btn--empty-left')
rangeHistory.max = textVersionsNb;
rangeHistory.value = textVersionsNb;
textVersions[textVersionsNb].classList.add('version--show');
textVersions[textVersionsNb].setAttribute('aria-hidden', 'false');
emptyBoxLeft.innerHTML = Number(rangeHistory.value) + 1;
rangeHistory.addEventListener("input", (el) => {
  baffleTxt.set({
    characters: 'RIMBAUD.ZAP',
    speed: 150,
  })
  .reveal(1000);
  textVersions.forEach((el) => {
    el.classList.remove('version--show');
    el.ariaHidden = "true";
    el.setAttribute('aria-hidden', 'true');
  });
  const activeVersion = rangeHistory.value;
  textVersions[activeVersion].classList.add('version--show');
  textVersions[activeVersion].setAttribute('aria-hidden', 'false');
  emptyBoxLeft.innerHTML = Number(rangeHistory.value) + 1;
  if (commitOn) {
    document.querySelector('.commit.show').classList.remove('show');
    const commit = document.querySelector('.version--show .commit');
    commit.classList.add('show');
  }
});

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

//
// Scrollbar with simplebar.js
//
let simpleBarContent = new SimpleBar(document.querySelector('.enfer'));
// Disable scroll simplebar for print
window.addEventListener('beforeprint', (event) => {
  simpleBarContent.unMount();
});
window.addEventListener('afterprint', (event) => {
  simpleBarContent = new SimpleBar(document.querySelector('.textes'));
});
