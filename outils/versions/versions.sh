#!/bin/sh

index=1
final="outils/versions/enfer-total.md"
> "$final"
for commit in $(git log --reverse --pretty=format:%h "$1")
do
  commitdate=$(git log -1 --date=format:'%d/%m/%Y à %H:%M:%S' --pretty=format:"%ad" "$commit")
  commitauthor=$(git log -1 --pretty=format:"%an" "$commit")
  commitmsg=$(git log -1 --pretty=format:"%s" "$commit")

  echo -e "version $index from commit $commit copied in $final"

  echo -e "<div class=\"version\" data-commit=\"${commit}\" data-version=\"${index}\" aria-hidden=\"true\">" >> "$final"
  echo -e "<div class=\"commit\">Voici la version ${index} de ce texte,<br>
  correspondant au commit ${commit},<br>datant du ${commitdate},<br>
  signé par&nbsp;${commitauthor},<br>et accompagné du message
  suivant :<br>« ${commitmsg} »." >> "$final"
  echo -e "</div>" >> "$final"
  echo -e "<div class=\"version__text\">\n" >> "$final"
  git show "$commit:./$1" >> "$final"
  echo -e "\n\n</div>" >> "$final"
  echo -e "\n</div>" >> "$final"
  let index++
done
sed -i -e '/^---$/,/^---$/d' "$final"
sed -i -e 's/<strong>/strong/g' "$final"
sed -i -e 's/<pre><code>/pre code/g' "$final"

